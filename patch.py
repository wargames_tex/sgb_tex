from wgexport import *
from pprint import pprint

# TODO:
# - Check with artillery indirect fire rules
# - Eliminated unit may not move in as reinforcements!
#
# Nice-to-have
# - limit number of reorg marks 
# 
doTutorial = False # Change this to false for final 
doLoaded   = True

moreHelp = '''
<html>
 <head>
  <title>{title}</title>
  <style>
  </style>
 </head>
 <body>
  <h1>{title}</h1>
  <h2>Content</h2>
  <ul>
   <li><a href="#rules>Rules</a></li>
   <li><a href="#setup>Optional rules and start turn</a></li>
   <li><a href="#turn">Turn and phase tracker</a></li>
   <li><a href="#reinf">Reinforcements</a></li>
   <li><a href="#battle">Battle declaration and resolution</a></li>
   <li><a href="#elim">Eliminated units</a></li>
   <li><a href="#vp">Victory points and conditions</a></li>
   <li><a href="#tut">Tutorial</a></li>
   <li><a href="#changes">Change log</a></li>
   <li><a href="#about">About this game</a></li>
   <li><a href="#credits">Credits</a></li>
  </ul>

  <h2><a name="rules"></a>Rules</h2>
  <p>
    A PDF of the rules is available through the <b>Help</b> menu.
    Please note that it may take a little while for your PDF viewer
    to launch.
  </p>

  <h2><img src="opt_Control.png" width=24 height=24><a name="setup"></a>
  &nbsp;Optional rules and start turn</h2>

  <center><img src="Optionals.png" width=300 height=300></center>

  <p> At the start of a new game, you will be presented with a window
   to select which optional rules should be in effect.  You will
   <i>not</i> be able to change these settings later in the game.
  </p>

  <p><img src="scenario_1.png" width=24 height=24> In this window you
    can also select the start turn to start from - either the 1st,
    2nd, or 3rd of July, 1863.</p>

  <p> When the game is progressed to the first phase (<b>Confederate
    movement</b> if enabled), then game pieces that are not in used
    are removed and the optional rules are locked. The units are also
    moved to their starting position according to the choice of start
    turn.  </p>

  <p> I you play by email, then one of you should select the optional
    rules, save the game <i> before</i> moving to the first real
    phase, and send it off to the other side for validation. Only then
    should you progress to the next phase.  </p>

  <p><b>Note</b>: If you start with the 2nd or 3rd of July set-up,
    then the turn numbers will still be reported as 1, 2, ..., but the
    turn track will be starting from the correct date.  This is
    because VASSAL does not allow one to skip turns in an efficient
    manner.</p>

  <p>The optional &quot;Loaded dice&quot; sets up the module to load
    non-uniform dice.  Essentially, the extreme results on either die
    is less likely.  Specifically, the probability table for each die
    is modified to be</p>

  <center>
    <table>
      <tr>
       <th>Die roll</th>
       <th>Frequency out of 18</th>
       <th>Probability</th>
      </tr>
      <tr><td> 1</td><td>1</td><td>~  6%</td></tr>
      <tr><td> 2</td><td>1</td><td>~  6%</td></tr>
      <tr><td> 3</td><td>2</td><td>~ 11%</td></tr>
      <tr><td> 4</td><td>2</td><td>~ 11%</td></tr>
      <tr><td> 5</td><td>3</td><td>~ 17%</td></tr>
      <tr><td> 6</td><td>3</td><td>~ 17%</td></tr>
      <tr><td> 7</td><td>2</td><td>~ 11%</td></tr>
      <tr><td> 8</td><td>2</td><td>~ 11%</td></tr>
      <tr><td> 9</td><td>1</td><td>~  6%</td></tr>
      <tr><td>10</td><td>1</td><td>~  6%</td></tr>
    </table>
  </center>

  <p>That means that in combat that the most likely roll difference is
    0 with a standard deviation of roughly 3.5.  The distribution of
    the roll difference tends toward a normal (bell) distribution,
    whereas the default distribution is triangular with a bit larger
    standard deviation.</p>

  <p>Use this option if you would like luck to play a little less of a
   role when determining the outcomes of battles. Note that we are
   still dealing with random numbers, which means extreme results
   <i>may</i> occur, so do not be surprised if they do. The modified
   dice means that dice results of +/-9 or +/-8 will happen a third
   less often. +/-7 or +/-6 half as often, and +/-5 about only
   marginally fewer times. It means an attack with a CF difference of
   +8 is a sure thing (no retreat possible), while an attack with -7
   is doomed to fail (defender will not be retreated). </p>

  <p>In this case, the dice will look different than normal so that it
    is evident to all that the dice are not regular dice.</p>


  <h2><a name="turn"></a>Turn and phase tracker</h2>

  <p> Use the <b>Turn tracker</b> interface in the menubar.  A number
    of automatic actions are implemented in this module, which heavily
    depend on the use of that interface.  </p>

  <p> You can go to the next phase by pressing the <b>+</b> button, or
    by pressing <code>Alt-T</code>.  This is the <i>only</i> possible
    way of moving the turn marker.</p>

  <p><b>Note</b>: The die-roll pop-up windows may steal focus from the
  main window, which means that key-board short-cuts, such as
  <code>Alt-T</code> are not registered.  Simply click the main window
  to bring that back into focus, and keyboard short cuts will be
  registered again.</p>

  <p> Phases that are not in use - because some optional rule was
    disabled - are automatically skipped.  </p>

  <h2><a name="reinf"></a>Reinforcements</h2>

  <p>Reinforcements units <i>are</i> moved on to the map
    automatically.  This can be disabled in the game preferences.</p>


  <h2><img src="battle-marker-icon.png" width=24 height=24/><a
   name="battle"></a> &nbsp;Battle declaration and resolution</h2>

  <p>Battles <i>must</i> be declared in a factions <b>movement</b>
    phase, including artillery <i>bombardments</i>.</p>

  <p>The module can automatically calculate odds and resolve combats
    via the use of <i>battle markers</i>.  To place a battle marker,
    select the attacking <i>and</i> defending units and press the
    battle marker button (<img src="battle-marker-icon.png"/>) or
    <code>Ctrl-X</code>.  This will place markers on the invovled
    units that identifiy the battle uniquely.</p>

  <center>
    <table>
      <tr>
        <td><img src="combat_1_select.png"></td>
        <td><img src="combat_2_odds.png"></td>
        <td><img src="combat_3_odds.png"></td>
      </tr>
      <tr>
       <td>Select attackers and defenders</td>
       <td>Press <code>Ctrl-X</code> to calculate odds</td>
       <td>Second battle</td>
      </tr>
    </table>
  </center>

  <p>If the user preference <b>Calculate Odds on battle
    declaration</b> is enabled (default), then the module will also
    calculate the odds (inluding bonuses and penalties for terrain),
    and place an odds marker (e.g., <img width=24 heigh=24
    src="odds_marker_0.png"/>) on one of the involved units.</p>

  <p>The rules says allow one to attack multiple hexes in a single
    combat.  The module takes that into account, as are the various
    defensive bonuses of from terrain and other features. </p>

  <p>If the user preference <b>Resolve battle results
    automatically</b> is enabled (default), then in the factions
    <b>Combat</b> phase, you can right click the odds marker and
    select <b>Resolve</b> (or <code>Ctrl-Y</code> or <img
    src="resolve-battles-icon.png" width=24 height=24> button) to
    resolve the combat.  The module will do the appropriate
    calculations and roll the dices needed, and a replace the odds
    marker with a <i>result marker</i> (e.g. <img width=24 height=24
    src="result_marker_DR.png"/>).</p>

  <p>Note that the dice windows may still the mouse focus.  In that
   case, simply click the main window (or any element in it), to put
   focus back there.</p>

  <center>
    <table>
      <tr>
        <td><img src="combat_4_result.png"></td>
        <td><img src="combat_5_implement.png"></td>
      </tr>
      <tr>
        <td><img src="combat_6_result.png"></td>
        <td><img src="combat_7_implement.png"></td>
      </tr>
      <tr>
        <td>Press <code>Ctrl-Y</code> for results</td>
        <td>Implement results</td>
      </tr>
    </table>
  </center>

  <p>The module will <i>not</i> apply the result to the invovled
    units. This <i>must</i> be done, according to the rules</i> by the
    factions.</p>

  <p>To clear a battle declaration, select one of the involved units
    or battle markers, and select <b>Clear</b> (or press
    <code>Ctrl-C</code>) from the context menu. All battles can be
    cleared by the <img src="clear-battles-icon.png"/> button in the
    menubar.</p>

  <h3>Artillery bombardments</h3>

  <p>If the optional rule on indirect artillery fire is enabled, then
  each factions turn has 3 phases, the second of which is for
  resolving artillery bombardments.  </p>

  <p>Remember (also see the rules)</p>
  <ul>

   <li>Only other artillery units may be attacked by indirect
     bombardment (enforced).</li>

   <li>Artillery units <i>must</i> have line of sight to <i>all</i>
    targets and be within range (one intermittent hex, (not
    enforced).</li>

   <li>Results <b>A2</b>, <b>A1</b>, and <b>AR</b> are ignored
     (enforced)</li>

   <li><b>D2</b> results are treated as <b>D1</b> (enforced)</li>

   <li>All artillery bombardments <i>must</i> be resolved before
     moving on to the combat phase</li>

  </ul>

  <h3><img
   src="remote.png" width=24 height=24> &nbsp;Defensive artillery support</h3>

  <p>If the optional rule on indirect artillery fire is enabled, then
   artillery units</p>
  <ul>
   <li><i>not</i> directly engaged by the enemy (not in enemy
     ZOC)</li>
   <li>which have <i>not</i> been the target of artillery
     bombardment</li>
   <li>and are within range of at least one attacker</li>
  </ul>
  <p><i>may</i> provide defensive indirect fire.</p>

  <p>To do that, the defending faction <i>must</i> declare that an
   artillery unit (cavalry or not) is providing defensive fire by
   select that unit and pressing <code>Ctrl-I</code> (or select
   <i>Indirect</i> from the context menu).  Failure to do so
   <i>will</i> result in wrong calculation of the combat
   differential.</p>

  <center>
    <table>
      <tr>
       <td><img src="indirect_1_start.png"></td>
       <td><img src="indirect_2_declare.png"></td>
       <td><img src="indirect_3_select.png"></td>
      </tr>
      <tr>
        <td>Situation, US combat phase</td>
        <td>Declare defensive indirect fire (<code>Ctrl-I</code>)</td>
        <td>Select <i>all</i> attackers and defenders, including
          artillery</td>
      </tr>
    </table>
  </center>

  <p>The artillery unit's defensive indirect file <i>must</i> declared
   to provide defense fire <i>before</i> calculating the combat
   differential (<code>Ctrl-X</code>).  When an artillery unit is
   declared to provide indirect defensive fire, then a red ring (<img
   src="remote.png" width=24 height=24>) will appear around the range
   factor.  This marker is cleared when the battle markers are cleared
   (Ctrl-C).</p>

  <center>
    <table>
      <tr>
       <td><img src="indirect_4_odds.png"></td>
       <td><img src="indirect_5_result.png"></td>
       <td><img src="indirect_6_implement.png"></td>
      </tr>
      <tr>
        <td>Calculate odds (<code>Ctrl-X</code>)</td>
        <td>Resolve combat (<code>Ctrl-Y</code>)</td>
        <td>Implement results, indirect fire defensive artillery does
          <i>not</i> retreat</td>
      </tr>
    </table>
  </center>

  <h3><img src="flip-icon.png" width=24 height=12/> &nbsp;Step losses</h3> 

  <p>Use the context menu item <b>Step Loss</b> (or
    <code>Ctrl-F</code> or <img src="flip-icon.png" width=24
    height=12> button) to implement step losses. Units that are has no
    step losses are automatically eliminated.</p>

  <h2><a name="elim"></a><img
    src="pool-icon.png" widht=24 height=24> &nbsp;Eliminated units</h2>

  <p>Eliminated units will be sent to a special holding board (<img
    src="pool-icon.png" widht=24 height=24>) so that the faction may
    tally the victory points at the end.</p>

  <p>Headquarter units are moved to the OOB (<code>Alt-B</code> or
  <img src="oob-icon.png" width=24 height=24>) from whence they came.

  <img src="reorganise.png" width=24 height=24> If the optional night
  turn rules are used, this allows a faction to restore those units to
  the map.</p>

  <h2><img src="vp-icon.png" height=24 width=36><a name="vp"></a>
  &nbsp;Victory points and conditions</h2>

 <p> The module can calculate the current number of victory points
  (VPs) each faction has by investigating </p>
 <ul>
  <li>Eliminated units (<code>Alt-E</code>),</li>
  <li>Units that has suffered a step loss, <i>and</i></li>
  <li>the current owner of the objective hexes.</li>
 </ul>

  <center>
    <img src="control.png"> <img src="control_flipped.png">
  </center>

  <p><b>Important:</b> The factions <i>must</i> make sure that the
  state of the control markers on the board (shown above) are
  up-to-date.  The module <i>does not</i> automatise this.  Thus, when
  the Confederate faction captures an objective hex (by occupying or
  passing through it), then that faction <i>must</i> make sure to flip
  the control marker (select and <code>Ctrl-F</code>, or from the
  right-click context menu).  Conversely, if the Union faction
  re-captures an objective hex, then that faction <i>must</i> flip the
  control marker back to its Union side.</p>
  

 <p> The module will periodically calculate the current victory points
   of both factions.  If you want to know the current status, click
   the victory point button (<img src="vp-icon.png" height=24
   width=36> or <code>Alt-V</code>) to get a tally in the chat log.
   </p>

  <p>The module will report sudden death conditions as pop-ups on
   start of the 2nd and 3rd of July.</p>
 
  <h2><a name="tut"></a>Tutorial</h2>

  <p>The tutorial in this module is based on the replay found in <i>The General</i> <b>Vol.15</b> #5, pages 6-14 which can be found at
  </p>


  <center>
    <code>https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2025%20No%205.pdf#page=6</code>
  </center>

  <p>
   Some notes:
  </p>

  <ul>

   <li>When stepping throught the tutorial, there are a number of
     steps being taken in the background.  If it seems that the
     tutorial is "stuck", try pressing Page-Down a few more times -
     the module is doing stuff in the background.</li>

   <li>Note that some of the moves are wrongly reported in that
     article, and they have, as far as possible, been corrected.</li>

   <li>The article mentions that the optional indirect fire optional
     rule is in use, but at no point during the game play does any of
     the factions make use of it, even though it would seem obvious to
     do so. I suspect the players have forgotten the rule (which is a
     little strange because the designer, Samuel Craig Taylor, Jr.,
     was the neutral onlooker).</li>

   <li>In terms of strategy it seems the Union faction made a bunch of
     mistakes during the game.  It gives up Cemetery Hill without much
     of a fight, and then the faction struggles to get back into the
     game from that point on. I have added a few comments in the
     tutorial where I see what I believe to be obvious mistakes.</li>

  </ul>

  <h2><a name="changes"></a>Change log</h2>
  <dl>
    <dt>1.0</dt>
    <dd>Original release of module</dd>
    <dt>1.1</dt>
    <dd>
      <ul>
        <li>Units and markers now on separate layers to allow users to
          get a better overview</li>
        <li>Line-of-Sight thread included</li>
      </ul>
    </dd>
    <dt>1.2</dt>
    <dd>
      <ul>
        <li>Drop shadows on chit - makes the whole thing look a lot
          better.</li>
        <li>Loaded dice option.</li>
        <li>More enforcements of rules on offensive indirect fire</li>
        <li>Help expanded with illustrations</li>
      </ul>
    </dd>
  </dl>

  <h2><a name="about"></a>About this game</h2>

  <p>Here, the game is called <b>Gettysburg - Smithsonian Edition</b>,
  but it is also sometimes named</p>
  <ul>
   <li>Gettysburg - 125<sup>th</sup> Anniversary Edition,</li>
   <li>Gettysburg - 1989 Edition, or</li>
   <li>Gettysburg - 1991 Edition, or</li>
  </ul>

  <p>There is no difference between these cames, save that they came
   in slightly different sized boxes.  All rules, graphics, text,
   counters, etc. are the same between these editions.</p>

  <p> This VASSAL module was created from L<sup>A</sup>T<sub>E</sub>X
   sources of a Print'n'Play version of the <b>Gettysburg</b> game.  That
   (a PDF) can be found at </p>

  <center>
    <code>https://gitlab.com/wargames_tex/sgb_tex</code>
  </center>

  <p> where this module, and the sources, can also be found.  The PDF
   can also be found as the rules of this game, available in the
   <b>Help</b> menu.  </p>

  <p> The original game was release by the Avalon Hill Game
   Company.</p>

  <h2><a name="credits"></a>Credits</h2>
  <dl>
   <dt>Design, development &amp; rules:</dt>
   <dd>Samuel Craig Taylor, Jr.</dd>
   <dt>Cover art:</dt><dd>George Parrish</dd>
   <dt>Graphic art, gameboard art, &amp; paste-up</dt>
   <dd>Charles Kibler</dd>
   <dt>Packaging:</dt><dd>Monarch Services &amp; Eastern Box</dd>
   <dt>Playtesters</dt>
   <dd>Fred Chatman, Robert Coggins, Mike Craighead, Dave Ferguson,
    Donald Greenwood, Jim Henson, Rex Martin, Bill Peschel, George
    Petronis, Edward Phillips, Ed Safley, &amp; Bruce Shelley</dd>
   <dt>Prep dept coordinator</dt>
   <dd>Lou Velenovsky</dd>
  </dl>

  <h2>Copyright and license</h2>

  <p> This work is &#127279; 2023 Christian Holm Christensen, and
   licensed under the Creative Commons Attribution-ShareAlike 4.0
   International License. To view a copy of this license, visit </p>

  <center>
   <code>http://creativecommons.org/licenses/by-sa/4.0</code>
  </center>

  <p>
    or send a letter to
  </p>

  <center>
   Creative Commons<br>
   PO Box 1866<br>
   Mountain View<br>
   CA 94042<br>
   USA
  </center>
</body>
</html>'''

# --------------------------------------------------------------------
def concatHexes(hexes):
    return ':'+':'.join(hexes) + ':'
# --------------------------------------------------------------------
def getHexes(hexes,what):
    return [k for k,v in hexes.items() if what in v]
# --------------------------------------------------------------------
def getWoodsHexes(hexes):
    return concatHexes(getHexes(hexes,'woods'))
# --------------------------------------------------------------------
def getRoughHexes(hexes):
    return concatHexes(getHexes(hexes,'rough'))
# --------------------------------------------------------------------
def getHillHexes(hexes):
    return concatHexes(getHexes(hexes,'hill'))
# --------------------------------------------------------------------
def getTownHexes(hexes):
    return concatHexes(getHexes(hexes,'city'))


# --------------------------------------------------------------------
def getRiverCrossings():
    # These are taken from the 'hexes.tex' file and then some
    # agressive Emacs qiuery-regexp-replace.
    # Meuse
    # from old import rivers
    from data import rivers
    edges = {
        ('NW','NE'): ('N',  lambda c,r:(c,r-1)),
        ('NE','E'):  ('NE', lambda c,r:(c+1,r-(c%2))),
        ('E','SE'):  ('SE', lambda c,r:(c+1,r+((c+1)%2))),
        ('SE','SW'): ('S',  lambda c,r:(c,r+1)),
        ('SW','W'):  ('SW', lambda c,r:(c-1,r+((c+1)%2))),
        ('W','NW'):  ('NW', lambda c,r:(c-1,r-(c%2)))}
    samecol = {
        'NW': 'SW',
        'NE': 'SE',
        'SW': 'NW',
        'SE': 'NE' }

    def getEdge(w1,w2):
        return edges.get((w1,w2),edges.get((w2,w1),None))
    def getTarget(edge,c1,r1):
        if edge is None:
            return ''
        targ = edge[1](c1,r1)
        if targ is None:
            return ''
        return hexName(targ[0],targ[1])
    def hexName(c,r):
        return f'{chr(c+ord("A")-1):1s}{r}'

    from re import match

    #foo = open('crossings.tex','w')
    #print(r'\begin{scope}[line width=2pt,red,'
    #      f'dash pattern=on 0mm off 5mm on 10mm off 5mm]',file=foo)
    
    crossing = []
    for river in rivers:
        for v1,v2 in zip(river[:-1],river[1:]):
            # print(f'{v1:6s}-{v2:6s} -> ',end='')
            h1 = v1[:v1.index('.')]
            h2 = v2[:v2.index('.')]
            s1 = match(r'([A-Z])([0-9]+)',h1)
            s2 = match(r'([A-Z])([0-9]+)',h2)
            c1 = ord(s1[1])-ord('A')+1
            c2 = ord(s2[1])-ord('A')+1
            r1 = int(s1[2])
            r2 = int(s2[2])
            w1 = v1[v1.index('.')+1:]
            w2 = v2[v2.index('.')+1:]
            edge = ['?','?']
            targ = '?'
            orig = '?'
            if h1 == h2:
                edge = getEdge(w1,w2)
                if edge is None:
                    raise RuntimeError(f'Edge {w1},{w2} ({v1},{v2}) not found')

                orig = h1
                targ = getTarget(edge,c1,r1)
            else:
                if c1 == c2: # Same column
                    ww2   = samecol.get(w2,None)
                    # print(f'{v1:6s} - {v2:6s} -> {w2}')
                    if ww2 is None:
                        h1 = h2
                        c1, c2 = c2, c1
                        r1, r2 = r2, r1
                        w1, w2 = w2, w1
                        ww2 = samecol[w2]
                    if ww2 is None:
                        raise RuntimeError(f'Same vertex {w2} not found')
                    w2 = ww2
                    edge = getEdge(w1,w2)
                    orig = h1
                    targ = getTarget(edge,c1,r1)
                else:
                    # Make sure we only look right 
                    if c2 < c1:
                        c1, c2 = c2, c1
                        r1, r2 = r2, r1
                        w1, w2 = w2, w1
                        h1, h2 = h2, h1
                        #print(f'<> {h1+"."+w1}-{h2+"."+w2} ', end='')

                    assert w1 != 'W',\
                        f'Invalid first corner {w1}'
                    assert w2 != 'E',\
                        f'Invalid second corner {w2}'

                    # Only take top edge 
                    if w1 in ['SE','SW']:
                        w1 =  samecol[w1]
                        r1 += 1
                        # print(f'[1 -> {hexName(c1,r1)+"."+w1:6s}] ',end=' ')

                    if w2 in ['SE','SW']:
                        w2 =  samecol[w2]
                        r2 += 1
                        # print(f'[2 -> {hexName(c2,r2)+"."+w2:6s}] ',end='')

                    # print(f'({hexName(c1,r1)+"."+w1}-'
                    #       f'{hexName(c2,r2)+"."+w2}) -> ',end='')
                    cc = c2
                    rr = r2
                    if w1 == 'NE':
                        w1  =  'W'
                        if w2 == 'NW' and (c1 % 2 == 0) and r1 != r2:
                            rr -= 1
                            w2 =  samecol[w2]
                        if w2 == 'NW' and (c1 % 2 == 1) and r1 == r2:
                            rr -= 1
                            w2 =  samecol[w2]
                        
                    elif w1 == 'E':
                        w1   = 'NW'
                    elif w1 == 'NW':
                        assert w2 == 'W',\
                            f'w1={w1} but w2={w2}'
                        cc   = c1
                        rr   = r1
                        w2   = 'NE'

                    # print(f'{hexName(cc,rr)} {w1:2s}-{w2:2s} ->',end='')
                    edge = getEdge(w1,w2)
                    orig = hexName(cc,rr)
                    targ = getTarget(edge,cc,rr)
                        
            crossing.append([orig,targ])
            # print(f'Crossing {orig:3s} -> {targ:3s} ({edge[0]})')
            # print(fr'  \draw[->] ({orig})--({targ});'
            #       f' % {v1:6s}--{v2:6s}',file=foo)

    # print(r'\end{scope}',file=foo)
    # foo.close()
    
    return ':'+':'.join([f'{h1}{h2}:{h2}{h1}' for h1,h2 in crossing])+':'

# --------------------------------------------------------------------
def getSame(typ,hexedges):
    edges = {
        'N':   ('S',  lambda c,r:(c,r-1)),
        'NE':  ('SW', lambda c,r:(c+1,r-(c%2))),
        'SE':  ('NW', lambda c,r:(c+1,r+((c+1)%2))),
        'S':   ('N',  lambda c,r:(c,r+1)),
        'SW':  ('NE', lambda c,r:(c-1,r+((c+1)%2))),
        'NW':  ('SE', lambda c,r:(c-1,r-(c%2)))}
    samecol = {
        'NW': 'SW',
        'NE': 'SE',
        'SW': 'NW',
        'SE': 'NE' }
    
    def getEdge(w1,w2):
        return edges.get((w1,w2),edges.get((w2,w1),None))
    def getTarget(edge,c1,r1):
        if edge is None:
            return ''
        targ = edge[1](c1,r1)
        if targ is None:
            return ''
        return hexName(targ[0],targ[1])
    def hexName(c,r):
        return f'{chr(c+ord("A")-1):1s}{r}'

    from re import match

    crossings = list()
    
    for hex, ter in hexedges.items():
        hexedgs  = ter.get(typ,None)
        if hexedgs is None and typ == 'hill':
            hexedgs = ter.get('rough',None)

        if hexedgs is None:
            continue

        hexcol = ord(hex[0])-ord('A')+1
        hexrow = int(hex[1:])
        #print(hex)
        for edge in hexedgs:
            tgt = edges.get(edge,None)
            if tgt is None:
                continue

            tgtEdge,tgtHex = tgt[0],hexName(*tgt[1](hexcol,hexrow))

            tgtTer = hexedges.get(tgtHex,None)
            if tgtTer is None:
                continue

            tgtTer = tgtTer.get(typ,
                                tgtTer.get('rough',{})
                                if typ=='hill' else {})
            if tgtEdge not in tgtTer:
                continue

            #cross = [hex,tgtHex]
            #cross.sort()

            crossings.append(''.join([hex,tgtHex]))
            
            #print(' ',edge,tgtHex,tgtEdge)

    #crossings = list(crossings)
    crossings.sort()

    ret = ':'+':'.join(crossings)+':'
    # print(ret)
    # for c in crossings:print(c)
    return ret

# --------------------------------------------------------------------
def addTurnPrototypes(n,
                      turns,
                      main,
                      prototypesContainer,
                      firstPhase,
                      marker='game turn',
                      zoneName='turns',
                      globalScenario='Scenario'):
    # ----------------------------------------------------------------
    # Loop over turns, and create prototype for game turn marker to
    # move it along the turn track.  We do that by adding commands to
    # the turn track, which then delegates to the prototype.
    #
    # We add the prototypes to the game turn marker later on.
    board = main['mapName']
    turnp = []
    for t in range(1,n+1):
        k = key(NONE,0)+f',Turn{t}'
        turns.addHotkey(hotkey       = k,
                        match        = (f'{{Turn=={t}&&'
                                        f'(Phase=="{firstPhase}")}}'),
                        reportFormat = (f'{{"=== <b>Turn "+'
                                        f'EffectiveTurn+" ({t})</b> ==="}}'),
                        name         = f'Turn {t}')
        main.addMassKey(name         = f'Turn marker to {t}',
                        buttonHotkey = k,
                        hotkey       = k,
                        buttonText   = '', 
                        target       = '',
                        filter       = (f'{{BasicName == "{marker}"}}'))
        pn     = f'To turn {t}'
        reg    = f'{{"Turn "+EffectiveTurn+"@turns"}}'
        traits = [
            SendtoTrait(mapName     = board,
                        boardName   = board,
                        name        = '',
                        restoreName = '',
                        restoreKey  = '',
                        zone        = zoneName,
                        destination = 'R', #R for region
                        region      = reg,
                        key         = k,
                        x           = 0,
                        y           = 0),
            #ReportTrait(k,
            #            report = f'~ Moving to turn {t}'),
            BasicTrait()]
        prototypesContainer.addPrototype(name        = f'{pn} prototype',
                                         description = f'{pn} prototype',
                                         traits      = traits)
        turnp.append(pn)

    return turnp

# --------------------------------------------------------------------
# Our main patching function
def patch(build,data,vmod,gverbose=False):
    # from os import getcwd
    from sys import path
    path.append('.')
    from pathlib import Path
    # print(getcwd())
    
    from re  import sub
    from PIL import Image
    from io  import BytesIO
    #from old import edges
    #from old import hexes
    from data import edges
    from data import hexes


    imgs = [
        '.imgs/combat_1_select.png',
        '.imgs/combat_2_odds.png',
        '.imgs/combat_3_odds.png',
        '.imgs/combat_4_result.png',
        '.imgs/combat_5_implement.png',
        '.imgs/combat_6_result.png',
        '.imgs/combat_7_implement.png',
        '.imgs/indirect_1_start.png',
        '.imgs/indirect_2_declare.png',
        '.imgs/indirect_3_select.png',
        '.imgs/indirect_4_odds.png',
        '.imgs/indirect_5_result.png',
        '.imgs/indirect_6_implement.png',
    ]
    for img in imgs:
        src = Path(img)
        dst = Path('images') / src.name
        vmod.addExternalFile(img,str(dst))

    game = build.getGame()

    
    # ================================================================
    #
    # Global properties
    #
    # ----------------------------------------------------------------
    gp                = game.getGlobalProperties()[0];
    # ----------------------------------------------------------------
    #
    # Lists of hexes and crossings
    #
    woodsHexes        = getWoodsHexes    (hexes)
    hillHexes         = getHillHexes     (hexes)
    townHexes         = getTownHexes     (hexes)
    roughHexes        = getRoughHexes    (hexes)
    woodsEdges        = getSame('woods',edges)
    hillEdges         = getSame('hill', edges)
    riverCrossing     = getRiverCrossings()
    globalWoods       = 'Woods'
    globalHill        = 'Hill'
    globalTown        = 'Town'
    globalRough       = 'Rough'
    globalRivers      = 'Rivers'
    globalWoodsEdges  = 'WoodsEdges'
    globalHillEdges   = 'HillEdges'
    
    for h,n in [[woodsHexes,        globalWoods     ],   
                [hillHexes,         globalHill      ],   
                [townHexes,         globalTown      ],   
                [roughHexes,        globalRough     ],   
                [riverCrossing,     globalRivers    ],
                [woodsEdges,        globalWoodsEdges],
                [hillEdges,         globalHillEdges ]]:
        gp.addProperty(name         = n,
                       initialValue = h,
                       description  = f'List of {n} hexes')
    

    # ----------------------------------------------------------------
    globalDeclare   = 'NotDeclare'
    globalResolve   = 'NotResolve'
    defenderHex     = 'DefenderHex'
    defenderWoods   = 'DefenderWoods'
    defenderHill    = 'DefenderHill'
    defenderTown    = 'DefenderTown'
    intoWoods       = 'IntoWoods'
    intoHill        = 'IntoHill'
    intoTown        = 'IntoTown'
    globalSetup     = 'NotSetup'
    isTutorial      = 'IsTutorial'
    globalScenario  = 'Scenario'
    globalTurn      = 'EffectiveTurn'
    globalVP        = 'VP'
    globalND        = 'wgNDefenders'
    globalNA        = 'wgNAttackers'
    notLoaded       = 'notLoaded'
    gp              = game.getGlobalProperties()[0];
    for no in range(1,7):
        gp.addProperty(name         = f'{defenderWoods}{no}',
                       initialValue = '',
                       description  = f'Defender woods {no}')
    for no in range(1,7):
        gp.addProperty(name         = f'{defenderHill}{no}',
                       initialValue = '',
                       description  = f'Defender hill {no}')
    for no in range(1,7):
        gp.addProperty(name         = f'{defenderTown}{no}',
                       initialValue = '',
                       description  = f'Defender town {no}')
    gp.addProperty(name         = globalDeclare,
                   initialValue = 'true',
                   description  = 'True when not in declare phase')
    gp.addProperty(name         = globalResolve,
                   initialValue = 'true',
                   description  = 'True when not in combat phase')
    gp.addProperty(name         = globalSetup,
                   initialValue = 'false',
                   description  = 'True when not in setup phase')
    gp.addProperty(name         = isTutorial,
                   initialValue = str(doTutorial).lower(), 
                   description  = 'Is tutorial loaded')
    gp.addProperty(name         = globalScenario,
                   initialValue = 1,
                   description  = 'Select setup scenario')
    gp.addProperty(name         = globalTurn,
                   initialValue = 1,
                   description  = 'Effective game turn')
    gp.addProperty(name         = globalND,
                   initialValue = 0,
                   description  = 'Number of defending units')
    gp.addProperty(name         = globalNA,
                   initialValue = 0,
                   description  = 'Number of attacking units')
    gp.addProperty(name         = notLoaded,
                   initialValue = True,
                   description  = 'True if not loaded dice')
    for faction in ['Confederate','Union']:
        gp.addProperty(name         = globalVP+faction,
                       initialValue = 0,
                       description  = f'{faction} VPs')
        


    # ================================================================
    #
    # Options
    #
    # ----------------------------------------------------------------
    optInitiative   = 'optInitiative'
    optControl      = 'optControl'
    optIndirect     = 'optIndirect'
    optNight        = 'optNight'
    optLoaded       = 'optLoaded'
    for o, t in [[optInitiative  , 'Initiative'],
                 [optControl     , 'Control'],
                 [optIndirect    , 'Indirect'],
                 [optNight       , 'Night'],
                 [optLoaded      , 'Loaded']
                 ]:
        gp.addProperty(name         = o,
                       initialValue = ('false' if o == optLoaded else 'true'),
                       description  = t)

    # ================================================================
    #
    # Keys, global properties
    #
    # ----------------------------------------------------------------
    updateHex          = key(NONE,0)+',updateHex'
    checkDeclare       = key(NONE,0)+',checkDeclare'
    checkResolve       = key(NONE,0)+',checkResolve'
    checkSetup         = key(NONE,0)+',checkSetup'
    calcOddsKey        = key(NONE,0)+',wgCalcBattleOdds'
    calcIdxKey         = key(NONE,0)+',wgCalcBattleIdx'
    calcFracKey        = key(NONE,0)+',wgCalcBattleFrac'
    calcAF             = key(NONE,0)+',wgCalcBattleAF'
    calcDF             = key(NONE,0)+',wgCalcBattleDF'
    dieRoll            = key(NONE,0)+',dieRoll'
    dicesRoll          = key(NONE,0)+',dicesRoll'
    flipTurn           = key(NONE,0)+',flipTurn'
    startUnion         = key(NONE,0)+',startUnion'    
    startConfederate   = key(NONE,0)+',startConfederate'
    startInitiative    = key(NONE,0)+',startInitiative'
    resolveKey         = key('Y')
    declareKey         = key('X')
    toggleKey          = key(NONE,0)+',toggleOpt'
    setOptional        = key(NONE,0)+',setOpt'
    cleanOptional      = key(NONE,0)+',cleanOpt'
    cleanUnits         = key(NONE,0)+',cleanUnits'
    fixOptional        = key(NONE,0)+',fixOpt'
    deleteKey          = key(NONE,0)+',delete'
    optKey             = key('Q',ALT)
    optShow            = key(NONE,0)+',optShow'
    nextPhase          = key('T',ALT) # key(NONE,0)+',nextPhase'    
    resetStep          = key(NONE,0)+',resetStep'
    toggleStep         = key(NONE,0)+',toggleStep'
    stepKey            = key('F')
    stepCmd            = key(NONE,0)+',stepLoss'
    reorgCmd           = key(NONE,0)+',reorganised'
    reorgKey           = key('O')
    eliminateKey       = key('E')
    eliminateCmd       = key(NONE,0)+',eliminate'
    stepOrElim         = key(NONE,0)+',stepOrElim'
    restoreKey         = key('R')
    updateVP           = key(NONE,0)+',updateVP'
    checkVP            = key(NONE,0)+',checkVP'
    tutorialKey        = key(NONE,0)+',isTutorial'
    autoReinforce      = 'gbAutoreinforce'
    debug              = 'wgDebug'
    verbose            = 'wgVerbose'
    hidden             = 'wg hidden unit'
    currentBattle      = 'wgCurrentBattle'
    battleUnit         = 'wgBattleUnit'
    battleCalc         = 'wgBattleCalc'
    battleMarker       = 'wgBattleMarker'
    battleCtrl         = 'wgBattleCtlr'
    battleNo           = 'wgBattleNo'
    battleResult       = 'wgBattleResult'
    battleShift        = 'wgBattleShift'
    battleFrac         = 'wgBattleFrac'
    battleAF           = 'wgBattleAF'
    battleDF           = 'wgBattleDF'
    gameTurn           = 'game turn'
    current            = 'Current'
    stackDx            = 12 # 'wgStackDx'
    stackDy            = 18 # 'wgStackDy'
    effTurn            = key(NONE,0)+',effectiveTurn'
    start1Key          = key(NONE,0)+',start1'
    start2Key          = key(NONE,0)+',start2'
    start3Key          = key(NONE,0)+',start3'
    setitupKey         = key(NONE,0)+',setitup'
    reinforceKey       = key(NONE,0)+',reinforce'
    remoteOn           = key(NONE,0)+',remoteOn'
    wgRollDice         = key(NONE,0)+',wgRollDice'
    rollDefaultDice    = key(NONE,0)+',wgRollDefaultDice'
    rollAltDice        = key(NONE,0)+',wgRollAltDice'

    # ================================================================
    #
    # Default preferences
    #
    go    = game.getGlobalOptions()[0]
    go.addBoolPreference(name = autoReinforce,
                         default = True,
                         desc    = 'Automatic reinforcements',
                         tab     = game['name'])
    prefs = go.getPreferences()    
    prefs[debug]          ['default'] = False
    prefs[verbose]        ['default'] = True
    prefs['wgAutoOdds']   ['default'] = True
    prefs['wgAutoResults']['default'] = True


    # ================================================================
    # Piece windows (remove)
    pwindows = game.getPieceWindows()
    # # print(pwindows)
    pwindows['Counters']['icon'] = 'unit-icon.png'
    

    # ================================================================
    #
    # Inventory
    #
    # ----------------------------------------------------------------
    # Add an inventory sorted on the unit supply and isolation status. 
    filt = '{Faction=="Confederate"||Faction=="Union"}'
    grp  = 'Faction,Step_Level'
    disp = ('{PropertyValue==Faction ? Faction : '
            '(Step_Level==1 ? "Full" : "Reduced")}')
    game.addInventory(include       = filt,
                      groupBy       = grp,
                      sortFormat    = '$PieceName$',
                      tooltip       = 'Show status of pieces',
                      nonLeafFormat = disp,
                      zoomOn        = True,
                      hotkey        = key('S',ALT),
                      refreshHotkey = key('S',ALT_SHIFT),
                      icon          = 'unit-icon.png')
    

    # ================================================================
    #
    # Dice
    #
    # ----------------------------------------------------------------
    # Dice
    diceName     = '1d10Dice'
    dicesName    = '2d10'
    diceKey      = key('0',ALT)
    dicesKey     = key('2',ALT)
    altDiceKey   = key('1',ALT)
    altDicesKey  = key('3',ALT)
    dices        = game.getSymbolicDices()
    toremove     = []
    print('Fixing up dice')
    for dn, dice in dices.items():
        print(f' Dice: {dn}')
        if dn == '1d10Dice':
            dice['hotkey'] = diceKey
        elif dn == 'Alt1d10Dice':
            dice['hotkey'] = altDiceKey
        elif dn.startswith('Alt'):
            dice['hotkey'] = altDicesKey
        else:
            dice['hotkey'] = dicesKey

        if doLoaded:
            dice['canDisable'] = True
            if not dn.startswith('Alt'):
                dice['propertyGate'] = optLoaded
            else:
                dice['propertyGate'] = notLoaded
                
        # Make our loaded dice 
        if not dn.startswith('Alt'): continue
        if not doLoaded:
            toremove.append(dice)
            continue

        prt = dice['tooltip']
        # print(f' Alternative dice "{prt}"')
        prt = prt.replace('Alt','')+' (loaded)'
        print(f' Alternative dice "{prt}"')              
        dice['tooltip'] = prt
        dice['format'] = f'{{"{prt}: "+result1}}'

        die = dice.getAllElements(SpecialDie,single=True)[0]
        die['name'] = prt
        faces = die.getAllElements(DieFace,single=False);
        toadd = []
        for face in faces:
            value = int(face['value'])
            if value < 3 or value > 8: continue;

            toadd.append(face)
            if value < 5 or value > 6: continue;

            toadd.append(face)

        for a in toadd:
            v = a['value']
            t = a['text']
            die.addFace(icon=a['icon'],text=a['text'],value=int(a['value']))

    for r in toremove:
        dices.remove(r)

    # ----------------------------------------------------------------
    prototypeContainer = game.getPrototypes()[0]
    # ----------------------------------------------------------------
    maps = game.getMaps()
    main = maps['Board']

    # ----------------------------------------------------------------
    # Delete key
    dkey = main.getMassKeys().get('Delete',None)
    if dkey is not None:
        dkey['icon'] = ''

    # ----------------------------------------------------------------
    # Layers
    pieceLayer      = 'PieceLayer'
    unitLayer       = 'Units'
    btlLayer        = 'Units' #'Battle' - doesn't work with auto odds
                              #if not same as units - anyways, the
                              #markers should stack with units.
    oddLayer        = 'Odds'
    resLayer        = 'Result'
    objLayer        = 'Objectives'

    layerNames = {objLayer:  {'t': objLayer,  'i': ''},
                  unitLayer: {'t': unitLayer, 'i': '' },
                  #btlLayer,
                  oddLayer: {'t': oddLayer+' markers', 'i': ''},
                  resLayer: {'t': resLayer+' markers', 'i': ''} }
                  
    layers = main.addLayers(description='Layers',
                            layerOrder=layerNames)
    for lt,ln in layerNames.items():
        layers.addControl(name       = lt,
                          tooltip    = f'Toggle display of {ln}',
                          text       = f'Toggle {ln["t"]}',
                          icon       = ln['i'],
                          layers     = [lt])
    layers.addControl(name    = 'Show all',
                      tooltip = f'Show all',
                      text    = f'Show all',
                      command = LayerControl.ENABLE,
                      layers  = list(layerNames.keys()))

    lmenu = main.addMenu(description = 'Toggle layers',
                         text        = '',
                         tooltip     = 'Toggle display of layers',
                         icon        = 'layer-icon.png',
                         menuItems   = ([f'Toggle {ln["t"]}'
                                         for ln in layerNames.values()]
                                        +['Show all']))

    hideP  = main.getHidePiecesButton()
    if len(hideP) > 0:
        print('Found hide pieces button, moving layers before that')
        main.remove(layers)
        main.remove(lmenu)
        main.insertBefore(layers,hideP[0])
        main.insertBefore(lmenu, hideP[0])
    else:
        print('Hide pueces button not found',hideP)
    
    # ----------------------------------------------------------------
    # LOS
    main.addLOS()
    
    # ----------------------------------------------------------------
    # Change stacking to be more tight since stacks may grow high
    stackmetrics = main.getStackMetrics(single=True)[0]
    stackmetrics['unexSepX'] = stackDx
    stackmetrics['unexSepY'] = stackDy
    stackmetrics['exSepX']   = int(2*stackDx)
    stackmetrics['exSepY']   = int(2*stackDy)
    main['color']            = rgb(255,215,79)
    main['color']            = rgb(200,0,0)
    # Zoom in a bit more on the counters - can be hard to read with
    # the small font used for names.
    main.getCounterDetailViewer()[0]['graphicsZoom'] = 1.5
    
    # ----------------------------------------------------------------
    # Extra documentation 
    doc  = game.getDocumentation()[0]
    more = doc.addHelpFile(title='More information',fileName='help/more.html')
    vmod.addFile('help/more.html', moreHelp.format(title=game['name']))

    tut = doc.getTutorials()
    tut['Tutorial']['name']          = 'Tutorial (The General Vol25, #5, p6)'
    tut['Tutorial']['promptMessage'] = 'Load the tutorial?'
    etut = doc.addTutorial(name='The General - Contest 147',
                           promptMessage = 'Load the The General contest 147?',
                           logfile       = 'Contest147.vlog')
    vmod.addExternalFile('Contest147.vlog', 'Contest147.vlog')
    doc.remove(etut)
    doc.insertBefore(etut,tut['Tutorial'])
    # doc.getHelpFiles()['Key bindings'])
    
    # ==================================================================
    oobs = game.getChartWindows()['OOBs']

    # ----------------------------------------------------------------
    # Get optionals map
    opts = maps['Optionals']
    opts['allowMultiple'] = False
    opts['markMoved'] = 'Never'
    opts['hotkey']    = optKey
    opts['launch']    = True
    opts['icon']      = 'opt-icon.png'
    opts.remove(opts.getImageSaver()[0])
    opts.remove(opts.getTextSaver()[0])
    opts.remove(opts.getGlobalMap()[0])
    opts.remove(opts.getHidePiecesButton()[0])
    obrd = opts.getBoardPicker()[0].getBoards()['Optionals']
    # print(obrd['width'],obrd['height'])
    # obrd['width']  = 500
    # obrd['height'] = 800
    okeys = opts.getMassKeys()
    # print(okeys)
    for kn,k in okeys.items():
        # print(f'Removing {kn} from Optionals')
        opts.remove(k) 
    ostart = opts.getAtStarts(False)
    for at in ostart:
        # print(at['name'])
        if at['name'] == hidden:
            # print(f'Removing {at["name"]} from Optionals')
            opts.remove(at)

    game.addStartupMassKey(name        = 'Optionals',
                           hotkey      = optKey,
                           target      = '',
                           filter      = f'{{BasicName=="{hidden}"}}',
                           whenToApply = StartupMassKey.START_GAME,
                           reportFormat=
                           f'{{{debug}?("~ Show optionals window"):""}}')
    game.addStartupMassKey(name        = 'Tutorial',
                           hotkey      = tutorialKey,
                           target      = '',
                           filter      = f'{{BasicName=="{hidden}"}}',
                           whenToApply = StartupMassKey.START_GAME,
                           reportFormat=
                           f'{{{debug}?("~ Show notes window "+{isTutorial}):""}}'                           )

    # ----------------------------------------------------------------
    # Get Zoned area
    zoned = main.getBoardPicker()[0].getBoards()['Board'].getZonedGrids()[0]

    # ----------------------------------------------------------------
    # Adjust the hexes
    #
    # Get the hex grid and turn of drawing
    print('Grids')
    zones  = zoned.getZones()
    hzone  = zones['hexes']
    hgrids = hzone.getHexGrids()
    hgrid  = hgrids[0]
    hnum   = hgrid.getNumbering()[0]
    print(f' grid offsets: h={hnum["hOff"]} v={hnum["vOff"]}')
    #hnum['hOff']  =  0
    hnum['vOff']  = int(hnum['vOff']) - 1

    # ================================================================
    #
    # Turn track
    #
    # ----------------------------------------------------------------
    turns         = game.getTurnTracks()['Turn']
    phaseNames    = ['Setup',                           # 0
                     'Confederate movement',            # 1
                     'Confederate artillery',           # 2
                     'Confederate combat',              # 3
                     'Union movement',                  # 4
                     'Union artillery',                 # 5
                     'Union combat'                     # 6
                     ]
    phases         = turns.getLists()['Phase']
    phases['list'] = ','.join(phaseNames)
    turns['reportFormat'] = '--- <b><i>$newTurn$</i></b> ---'
    csMove         = phaseNames[1]
    csArt          = phaseNames[2]
    csCombat       = phaseNames[3]
    usMove         = phaseNames[4]
    usArt          = phaseNames[5]
    usCombat       = phaseNames[6]

    # ----------------------------------------------------------------
    # Specific actions 
    turns.addHotkey(hotkey       = fixOptional,
                    match        = f'{{Phase=="{csMove}"&&Turn==1}}',
                    reportFormat = (f'{{{debug}?"~Cleaning for optional rules":'
                                    f'""}}'),
                    name         = 'CleanOptional')
    turns.addHotkey(hotkey       = setitupKey,
                    match        = f'{{Phase=="{csMove}"&&Turn==1}}',
                    reportFormat = (f'{{{debug}?"~Cleaning for optional rules":'
                                    f'""}}'),
                    name         = 'SetItUp')
    turns.addHotkey(hotkey       = effTurn,
                    match        = f'{{Phase=="{phaseNames[0]}"}}',
                    reportFormat = (f'{{{debug}?("--- <b>Effective Turn "+'
                                    f'EffectiveTurn+"</b> ---"):""}}'),
                    name         = 'Effective turn')
    turns.addHotkey(hotkey       = flipTurn,
                    match        = f'{{Turn!=1&&Phase=="{csMove}"}}',
                    reportFormat = '--- <b>Confederate Turn</b> ---',
                    name         = 'Confederate Turn')
    turns.addHotkey(hotkey       = flipTurn,
                    match        = f'{{Phase=="{usMove}"}}',
                    reportFormat = '--- <b>Union Turn</b> ---',
                    name         = 'Union Turn')
    turns.addHotkey(hotkey       = reinforceKey+'CS',
                    match        = f'{{{autoReinforce}&&Phase=="{csMove}"}}',
                    reportFormat = '--- <i>Reinforce Confederate faction</i> ---',
                    name         = 'Confederate reinforcements')
    turns.addHotkey(hotkey       = reinforceKey+'US',
                    match        = f'{{{autoReinforce}&&Phase=="{usMove}"}}',
                    reportFormat = '--- <b>Reinforce Union faction</b> ---',
                    name         = 'Union reinforcements')
    turns.addHotkey(hotkey       = reorgCmd+'CS',
                    match        = f'{{{optNight}&&Phase=="{csMove}"&&(Turn%8==0)}}',
                    reportFormat = '--- <i>Reorganise Confederate units</i> ---',
                    name         = 'Confederate reorganise')
    turns.addHotkey(hotkey       = reorgCmd+'US',
                    match        = f'{{{optNight}&&Phase=="{usMove}"&&(Turn%8==0)}}',
                    reportFormat = '--- <b>Reorganise Union units</b> ---',
                    name         = 'Union reorgnaise')
    # --- Add commands to turn track to do stuff, and provide reminders
    turns.addHotkey(hotkey = checkDeclare,
                    name   = 'Check for (any) declare phase')
    turns.addHotkey(hotkey = checkResolve,
                    name   = 'Check for (any) combat phase')
    turns.addHotkey(hotkey = checkSetup,
                    match  = '{Turn==1}',
                    name   = 'Check for setup phase')
    turns.addHotkey(hotkey = nextPhase,
                    match  = '{Phase=="Setup"&&Turn>1}',
                    name   = 'Skip setup phase on later turns')
    turns.addHotkey(hotkey = nextPhase,
                    match  = (f'{{(Phase=="{csMove}"||'
                              f'Phase=="{csCombat}"||'
                              f'Phase=="{csArt}"||'
                              f'Phase=="{usArt}"||'
                              f'Phase=="{usCombat}")&&'
                              f'EffectiveTurn==1}}'),
                    name   = 'Skip initial two CS phases w/nothing to do')
    turns.addHotkey(hotkey = nextPhase,
                    match  = (f'{{Phase.contains("artillery")&&'
                              f'!{optIndirect}}}'),
                    name   = 'Skip indirect fire phases if not enabled')
    turns.addHotkey(hotkey = '',
                    match  = '{(Turn%8)==0&&Phase.contains("movement")}',
                    name   = 'Night turn reminder',
                    reportFormat = (f'{{{verbose}?("`<b>Night turn</b>. "+'
                                    f'"<i>Must</i> disengage from enemy ZOC. "+'
                                    f'"<i>Cannot</i> engage enemy ZOC. "):""}}'))
    # Victory conditions
    turns.addHotkey(hotkey = checkVP,
                    match  = (f'{{((EffectiveTurn==9||'
                              f'EffectiveTurn==17||'
                              f'EffectiveTurn==25)&&'
                              f'Phase=="{csMove}")}}'),
                    reportFormat = (f'{{{debug}?("Check for victory"):""}}'))

    # ----------------------------------------------------------------
    # Turn track progress 
    turnp = addTurnPrototypes(24,turns,main,prototypeContainer,
                              csMove, #phaseNames[0],
                              marker=gameTurn, zoneName='Turn track',
                              globalScenario=globalScenario)

    # --- Clear markers phases ---------------------------------------
    keys = turns.getHotkeys(asdict=True)
    keys['Clear battle markers']['match'] = (
        f'{{Phase=="{csMove}"'
        f'||Phase=="{usMove}"'
        f'}}')

    # ================================================================
    #
    # Global key commands
    #
    # ----------------------------------------------------------------
    curBtl        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true}}')
    curDef        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true&&'
                     f'IsAttacker==false}}')
    curAtt        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true&&'
                     f'IsAttacker==true}}')
    keys                     = main.getMassKeys()
    userMark                 = keys['User mark battle']
    selMark                  = keys['Selected mark battle']
    resMark                  = keys['Selected resolve battle']
    userMark['canDisable']   = True
    userMark['propertyGate'] = globalDeclare
    selMark ['canDisable']   = True
    selMark ['propertyGate'] = globalDeclare
    resMark ['canDisable']   = True
    resMark ['propertyGate'] = globalResolve
    
    # ----------------------------------------------------------------
    # Add some global keys to the map
    #
    # - Flip turn marker (Ctrl+Shift+F)
    # - Mark twice out-of-supply Allied units (Ctrl+Shift+I)
    # - Eliminate twice isolated Allied units (Ctrl+Shift+E)
    main.addMassKey(name         = 'Flip turn marker',
                    buttonHotkey = flipTurn,
                    hotkey       = stepKey,
                    buttonText   = '', 
                    target       = '',
                    singleMap    = True,
                    filter       = f'{{BasicName == "{gameTurn}"}}')
    main.addMassKey(name         = 'Set effective turn',
                    buttonHotkey = effTurn,
                    hotkey       = effTurn,
                    buttonText   = '',
                    target       = '',
                    singleMap    = True,
                    filter       = f'{{BasicName == "{gameTurn}"}}')
    main.addMassKey(name         = 'Reinforce confederate faction',
                    buttonHotkey = reinforceKey+'CS',
                    hotkey       = reinforceKey,
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    filter       = f'{{Faction == "Confederate"}}',
                    #reportFormat = 'Reinforce CS'
                    )
    main.addMassKey(name         = 'Reinforce union faction',
                    buttonHotkey = reinforceKey+'US',
                    hotkey       = reinforceKey,
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    filter       = f'{{Faction == "Union"}}',
                    #reportFormat = 'Reinforce US'
                    )
    main.addMassKey(name         = 'Reorganise confederate units',
                    buttonHotkey = reorgCmd+'CS',
                    hotkey       = reorgCmd+'Finish',
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    filter       = (f'{{Faction == "Confederate"&&'
                                    f'Rest_Level==2}}'),
                    reportFormat = 'Reorganise CS'
                    )
    main.addMassKey(name         = 'Reorganise confederate units',
                    buttonHotkey = reorgCmd+'US',
                    hotkey       = reorgCmd+'Finish',
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    filter       = (f'{{Faction == "Union"&&'
                                    f'Rest_Level==2}}'),
                    reportFormat = 'Reorganise US'
                    )
                    
    # ----------------------------------------------------------------
    # - Historical setups
    main.addMassKey(name         = 'Jul 1 setup',
                    icon         = '',#'german-icon.png',
                    buttonHotkey = start1Key, #key('G',CTRL_SHIFT),
                    hotkey       = start1Key,
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    canDisable   = False,
                    #propertyGate = '{{Phase=="Setup"}}',
                    tooltip      = 'Load 1st of July setup',
                    reportFormat = '` Moving all units to start-up')
    main.addMassKey(name         = 'Jul 2 setup',
                    icon         = '',#'allied-icon.png',
                    buttonHotkey = start2Key, #key('A',CTRL_SHIFT),
                    hotkey       = start2Key,
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    canDisable   = False,
                    #propertyGate = '{{Phase=="Setup"}}',
                    tooltip      = 'Load 2nd of July setup',
                    reportFormat = '~ Moving all units to start-up')
    main.addMassKey(name         = 'Jul 3 setup',
                    icon         = '',#'allied-icon.png',
                    buttonHotkey = start3Key, #key('A',CTRL_SHIFT),
                    hotkey       = start3Key,
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    canDisable   = False,
                    #propertyGate = '{{Phase=="Setup"}}',
                    tooltip      = 'Load 3rd of July setup',
                    reportFormat = '~ Moving all units to start-up')
    # ----------------------------------------------------------------
    # - Check for declaration phases 
    main.addMassKey(name         = 'Check for declare phase',
                    buttonHotkey = checkDeclare,
                    buttonText   = '',
                    hotkey       = checkDeclare,
                    singleMap    = True,
                    target       = '',
                    filter       = f'{{BasicName=="{hidden}"}}',
                    reportFormat = (f'{{{debug}?("~ Check for declare"):""}}'))
    # ----------------------------------------------------------------
    # - Check for combat phase 
    main.addMassKey(name         = 'Check for combat phase',
                    buttonHotkey = checkResolve,
                    buttonText   = '',
                    hotkey       = checkResolve,
                    singleMap    = True,
                    target       = '',
                    filter       = f'{{BasicName=="{hidden}"}}',
                    reportFormat = (f'{{{debug}?("~ Check for resolve"):""}}'))
    # ----------------------------------------------------------------
    # - Store defender hex
    repWoods     = '+","+'.join([f'{defenderWoods}{no}' for no in range(1,7)])
    repHill      = '+","+'.join([f'{defenderHill}{no}'  for no in range(1,7)])
    repTown      = '+","+'.join([f'{defenderTown}{no}'  for no in range(1,7)])
    main.addMassKey(name         = 'Store defender hex',
                    buttonHotkey = updateHex,
                    buttonText   = '',
                    singleMap    = True,
                    hotkey       = updateHex,
                    target       = '',
                    filter       = curDef,
                    reportFormat = (f'{{{debug}?("Mass update defender hex"):""}}'))
    main.addMassKey(name         = 'Store attacker hex',
                    buttonHotkey = updateHex,
                    buttonText   = '',
                    singleMap    = True,
                    hotkey       = updateHex,
                    target       = '',
                    filter       = curAtt,
                    reportFormat = (f'{{{debug}?("Mass update attacker hex"):""}}'))
    # ----------------------------------------------------------------
    # - Clean counters based on optional flags 
    main.addMassKey(name         = 'Clean optional initiative',
                    buttonHotkey = fixOptional,
                    buttonText   = '',
                    hotkey       = fixOptional,
                    singleMap    = True,
                    target       = '',
                    filter       = (f'{{BasicName=="{gameTurn}"}}'),
                    reportFormat = (f'{{{debug}?("~ Fix up optionals"):""}}'))
    main.addMassKey(name         = 'Clean optional initiative',
                    buttonHotkey = cleanOptional,
                    buttonText   = '',
                    hotkey       = deleteKey,
                    singleMap    = False,
                    target       = '',
                    filter       = (f'{{{optInitiative}==false&&'
                                    f'BasicName=="initiative"}}'),
                    reportFormat = (f'{{{debug}?("~ Clear initiative "+'
                                    f'({optInitiative}?"no":"yes")):""}}'))
    main.addMassKey(name         = 'Clean unused units',
                    buttonHotkey = cleanUnits,
                    buttonText   = '',
                    hotkey       = deleteKey,
                    singleMap    = False,
                    target       = '',
                    filter       = (f'{{UnitScenario!={globalScenario}}}'),
                    reportFormat = (f'{{{debug}?("~ Clear unused units"):""}}'))
    main.addMassKey(name         = 'Set it up',
                    buttonHotkey = setitupKey,
                    buttonText   = '',
                    hotkey       = setitupKey,
                    singleMap    = False,
                    target       = '',
                    filter       = (f'{{BasicName=="{gameTurn}"}}'),
                    reportFormat = (f'{{{debug}?("~ Set-up pieces"):""}}'))
    # ================================================================
    #
    # VPs
    #
    main.addMassKey(name         = 'VPs',
                    icon         = 'vp-icon.png',
                    buttonText   = '',
                    buttonHotkey = key('V',ALT),
                    hotkey       = updateVP+'Turn',
                    tooltip      = 'Calculate and report current VPs',
                    singleMap    = True,
                    target       = '',
                    filter       = f'{{BasicName=="{gameTurn}"}}',
                    reportFormat = (f'{{{debug}?"!Preparing update of VPs":'
                                    f'""}}'))
    main.addMassKey(name         = 'VPs',
                    buttonHotkey = updateVP+'Trampoline',
                    hotkey       = updateVP,
                    singleMap    = False,
                    target       = '',
                    filter       = f'{{BasicName!="{gameTurn}"}}')
    main.addMassKey(name         = 'Victory',
                    buttonHotkey = checkVP,
                    hotkey       = checkVP,
                    singleMap    = True,
                    target       = '',
                    filter       = f'{{BasicName=="{gameTurn}"}}')
    # ================================================================
    # Forward to selected
    main.addMassKey(name         = 'Reorganise',
                    buttonHotkey = reorgKey,
                    hotkey       = reorgKey)
    main.addMassKey(name         = 'Indirect',
                    buttonHotkey = remoteOn,
                    hotkey       = remoteOn)

    # ================================================================
    #
    # Prototypes
    #
    # ----------------------------------------------------------------
    # Clean up prototypes
    prototypes         = prototypeContainer.getPrototypes(asdict=False)
    seen       = list()
    for p in prototypes:
        if p['name'] == ' prototype':
            prototypes.remove(p)
        if p['name'] in ['artillery reconnaissance prototype']:
            prototypes.remove(p)
                         
        if p['name'] in seen:
            # print(f'Removing prototype {p["name"]}')
            prototypes.remove(p)
        seen.append(p['name'])

    prototypes     = prototypeContainer.getPrototypes(asdict=True)

    # ----------------------------------------------------------------
    # Modify Markers prototype
    markersP     = prototypes['Markers prototype']
    traits       = markersP.getTraits()
    mdel         = Trait.findTrait(traits,DeleteTrait.ID)
    mdel['key']  = deleteKey
    mdel['name'] = ''        
    markersP.setTraits(*traits)

    # ----------------------------------------------------------------
    # Modify odds prototype
    oddsP          = prototypes['OddsMarkers prototype']
    traits         = oddsP.getTraits()
    basic          = traits.pop()
    die            = Trait.findTrait(traits,CalculatedTrait.ID,
                                     key='name',value='Die')
    roll           = Trait.findTrait(traits,GlobalHotkeyTrait.ID,
                                     key='globalHotkey',
                                     value=key('6',ALT))
    res           = Trait.findTrait(traits,CalculatedTrait.ID,
                                     key='name',value='BattleResult')
    trg           = Trait.findTrait(traits,TriggerTrait.ID,
                                     key='key',value=resolveKey)
    arty          = 'Phase.contains("artillery")'
    narty         = '!Phase.contains("artillery")'
    uarty         = 'Type.contains("artillery")'
    reg           = (f'(Die+{battleFrac})>=6?({arty}?"D1":"D2"):'
                     f'(Die+{battleFrac})>=3?"D1":'
                     f'(Die+{battleFrac})>=1?"DR":'
                     f'(Die+{battleFrac})>=-2?({arty}?"":"AR"):'
                     f'(Die+{battleFrac})>=-5?({arty}?"":"A1"):'
                     f'({arty}?"":"A2")')
    rep            = None

    for t in traits:
        if t.ID != ReportTrait.ID: continue
        if not t['report'].startswith('{"` Battle # '): continue
        rep = t
        break

    print('Battle report')
    rep['report'] = rep['report'][:-1]+f'+" ("+(Die+{battleFrac})+")"}}'
    print(f"->  {rep['report']}")
    getUsRoll  = 'UnionDice_result'
    getCsRoll  = 'ConfederateDice_result'

    
    if doLoaded:
        altRoll      = GlobalHotkeyTrait(key=rollAltDice,
                                         globalHotkey = altDicesKey,
                                         description = 'Roll alternative dice')
        trigRoll     = TriggerTrait(name = 'Roll normal dice',
                                    key=wgRollDice,
                                    actionKeys = [rollDefaultDice],
                                    property = f'{{!{optLoaded}}}')
        trigAltRoll  = TriggerTrait(name = 'Roll alternative dice',
                                    key=wgRollDice,
                                    actionKeys = [rollAltDice],
                                    property = f'{{{optLoaded}}}')
        traits.insert(traits.index(roll),altRoll);
        traits.insert(traits.index(altRoll),trigRoll)
        traits.insert(traits.index(altRoll),trigAltRoll)
        traits.insert(traits.index(altRoll),
                      ReportTrait(key(NONE,0)+'wgDefaultRollDice',
                                  report='Rolling normal dice'))
        traits.insert(traits.index(altRoll),
                      ReportTrait(key(NONE,0)+'wgAltRollDice',
                                  report='Rolling alternative dice'))

        roll['key']  = rollDefaultDice
        getUsRoll    = f'({optLoaded}?Alt{getUsRoll}:{getUsRoll})'
        getCsRoll    = f'({optLoaded}?Alt{getCsRoll}:{getCsRoll})'
        
    roll['globalHotkey'] = dicesKey
    getRoll              = f'GetString("{dicesName}_result")'
    getRoll1             = f'(Phase.contains("Union")?{getUsRoll}:{getCsRoll})'
    getRoll2             = f'(Phase.contains("Confederate")?{getUsRoll}:{getCsRoll})'
    parseInt             = 'Integer.parseInt'
    die['expression'] = (f'{{{parseInt}({getRoll1})-{parseInt}({getRoll2})}}')
    die['expression'] = (f'{{{getRoll1}-{getRoll2}}}')
    res['expression'] = (f'{{({reg})}}')

    traits.insert(0,
                  RestrictCommandsTrait(
                      name          = 'Restrict Ctrl-Y to combat or artillery',
                      hideOrDisable = RestrictCommandsTrait.DISABLE,
                      expression    = (f'{{{globalResolve}==true}}'),
                      keys          = [resolveKey]))
    traits.insert(0,
                  RestrictCommandsTrait(
                      name          = 'Restrict assign result to combat or artillery',
                      hideOrDisable = RestrictCommandsTrait.DISABLE,
                      expression    = (f'{{{globalResolve}==true}}'),
                      keys          = [key(NONE,0)+f',wgMarkResult{d}real'
                                       for d in range(1,7)]))

    traits.extend([
        MarkTrait(name = pieceLayer, value = oddLayer),
        CalculatedTrait(name       = 'Die1',
                        # expression = f'{{{parseInt}({getAlRoll})}}',
                        expression  = f'{{{getUsRoll}}}',
                        description = 'Take first die'),
        basic])
    oddsP.setTraits(*traits)

    # ----------------------------------------------------------------
    # Modify odds prototype
    btlP   = prototypes['BattleMarkers prototype']
    traits = btlP.getTraits()
    basic  = traits.pop()
    traits.append(MarkTrait(name = pieceLayer, value = btlLayer))
    btlP.setTraits(*traits,basic)
    
    # ----------------------------------------------------------------
    # Modify odds prototype
    resP   = prototypes['ResultMarkers prototype']
    traits = resP.getTraits()
    basic  = traits.pop()
    traits.append(MarkTrait(name = pieceLayer, value = resLayer))
    resP.setTraits(*traits,basic)
    
    # ----------------------------------------------------------------
    # Modify optionals prototype
    optionalP = prototypes['OptionalMarkers prototype']
    traits    = optionalP.getTraits()
    basic     = traits.pop()
    optionalP.setTraits(
        TriggerTrait(name       = '',
                     key        = toggleKey,
                     actionKeys = [setOptional],
                     property   = f'{{Phase=="Setup"}}'),
        ClickTrait(key = toggleKey,
                   context = False,
                   whole = True,
                   description = 'Toggle optional rule'),
        ReportTrait(setOptional,
                    report=(f'{{{debug}?("! "+BasicName+" toggle "+'
                            f'Name+" "+'
                            f'(Step_Level==1?"On":"Off")):""}}')),
        basic)
    # ----------------------------------------------------------------
    # Modify controls prototype
    controlP  = prototypes['ControlMarkers prototype']
    traits    = controlP.getTraits()
    basic     = traits.pop()
    controlP.setTraits(
        MarkTrait(
            name       = pieceLayer,
            value      = objLayer),
        CalculatedTrait(
            name       = 'Control',
            expression = '{Step_Level}'),
        CalculatedTrait(
            name       = 'Faction',
            expression = '{(Control==2)?"Confederate":"Union"}'),
        NoStackTrait(move=NoStackTrait.NEVER_MOVE,
                     bandSelect=NoStackTrait.NEVER_BAND_SELECT),
        GlobalPropertyTrait(
            ['',updateVP+'US',GlobalPropertyTrait.DIRECT,
             f'{{{globalVP}Union+VP}}'],
            name    = f'{globalVP}Union',
            numeric = True),
        GlobalPropertyTrait(
            ['',updateVP+'CS',GlobalPropertyTrait.DIRECT,
             f'{{{globalVP}Confederate+VP}}'],
            name    = f'{globalVP}Confederate',
            numeric = True),
        TriggerTrait(
            name       = 'Update Union VPs',
            command    = '',
            key        = updateVP,
            actionKeys = [updateVP+'US'],
            property   = '{Faction=="Union"}'),
        TriggerTrait(
            name       = 'Update Confederate VPs',
            command    = '',
            key        = updateVP,
            actionKeys = [updateVP+'CS'],
            property   = '{Faction=="Confederate"}'),
        ReportTrait(updateVP+'CS',
                    report=(f'{{{verbose}?("!"+LocationName+" ("+VP+")"+'
                            f'" under "+Faction+" control -> "+'
                            f'{globalVP}Confederate):""}}')),
        ReportTrait(updateVP+'US',
                    report=(f'{{{verbose}?("!"+LocationName+" ("+VP+")"+'
                            f'" under "+Faction+" control -> "+'
                            f'{globalVP}Union):""}}')),
        basic)
    
    # ----------------------------------------------------------------
    # Create die roller prototype
    traits = [
        CalculatedTrait(
            name = 'Die',
            expression = f'{{GetProperty("{diceName}_result")}}',
            description = 'Die roll'),
        GlobalHotkeyTrait(
            name         = '',
            key          = dieRoll,
            globalHotkey = diceKey,
            description  = 'Roll dice'),
        ReportTrait(diceKey,
                    report=f'{{{verbose}?("Rolled dice {diceName}"):""}}'),
        BasicTrait()
    ]
    prototypeContainer.addPrototype(name        = f'Dice prototype',
                                    description = f'Dice prototype',
                                    traits      = traits)

    # ----------------------------------------------------------------
    # Create dices roller prototype
    traits = [
        CalculatedTrait(
            name = 'UnionDie',
            expression = f'{{GetProperty("UnionDice_result")}}',
            description = 'Die roll'),
        CalculatedTrait(
            name = 'ConfederateDie',
            expression = f'{{GetProperty("ConfederateDice_result")}}',
            description = 'Die roll'),
        GlobalHotkeyTrait(
            name         = '',
            key          = dicesRoll,
            globalHotkey = dicesKey,
            description  = 'Roll dices'),
        ReportTrait(diceKey,
                    report=f'{{{debug}?("Rolled dices {dicesName}"):""}}'),
        BasicTrait()
    ]
    prototypeContainer.addPrototype(name        = f'Dices prototype',
                                    description = f'Dices prototype',
                                    traits      = traits)

    # ----------------------------------------------------------------
    #
    # Setup prototype 
    traits = [
        CalculatedTrait(
            name        = 'EffTurn',
            expression  = (f'{{Turn+'
                           f'({globalScenario}==2?8:'
                           f'({globalScenario}==3?16:0))}}'),
            description = 'Effective turn'),
        GlobalPropertyTrait(
            ['',fixOptional+'Control',GlobalPropertyTrait.DIRECT,
             f'{{{optNight}?{optNight}:{optControl}}}'],
            name = optControl,
            numeric = True),
        GlobalPropertyTrait(
            ['',fixOptional+'Loaded',GlobalPropertyTrait.DIRECT,
             f'{{!{optLoaded}}}'],
            name = notLoaded,
            numeric = True),
        GlobalPropertyTrait(
            ['',effTurn,GlobalPropertyTrait.DIRECT,
             f'{{EffTurn}}'],
            name = globalTurn,
            numeric = True),
        GlobalHotkeyTrait(name         = '',
                          key          = start1Key+'Global',
                          globalHotkey = start1Key),
        GlobalHotkeyTrait(name         = '',
                          key          = start2Key+'Global',
                          globalHotkey = start2Key),
        GlobalHotkeyTrait(name         = '',
                          key          = start3Key+'Global',
                          globalHotkey = start3Key),
        GlobalHotkeyTrait(name         = '',
                          key          = fixOptional+'Global',
                          globalHotkey = cleanOptional),
        GlobalHotkeyTrait(name         = '',
                          key          = cleanUnits+'Global',
                          globalHotkey = cleanUnits),
        TriggerTrait(name       = 'Move pieces',
                     command    = '',
                     key        = setitupKey,
                     property   = f'{{{globalScenario}==1}}',
                     actionKeys = [start1Key+'Global']),
        TriggerTrait(name       = 'Move pieces',
                     command    = '',
                     key        = setitupKey,
                     property   = f'{{{globalScenario}==2}}',
                     actionKeys = [start2Key+'Global']),
        TriggerTrait(name       = 'Move pieces',
                     command    = '',
                     key        = setitupKey,
                     property   = f'{{{globalScenario}==3}}',
                     actionKeys = [start3Key+'Global']),
        TriggerTrait(name       = 'Fix optional rules',
                     command    = '',
                     key        = fixOptional,
                     actionKeys = [effTurn,
                                   fixOptional+'Control',
                                   fixOptional+'Loaded',
                                   cleanUnits+'Global']),
        ReportTrait(setitupKey,
                    report=(f'{{{debug}?("~ Setting up pieces for Jul "+'
                            f'{globalScenario}+" starting turn"):""}}')),
        ReportTrait(start1Key+'Global',
                    report=(f'{{{debug}?("~ Global Jul "+'
                            f'{globalScenario}+" starting turn"):""}}')),
        ReportTrait(start2Key+'Global',
                    report=(f'{{{debug}?("~ Global Jul "+'
                            f'{globalScenario}+" starting turn"):""}}')),
        ReportTrait(start3Key+'Global',
                    report=(f'{{{debug}?("~ Global Jul "+'
                            f'{globalScenario}+" starting turn"):""}}')),
        ReportTrait(cleanOptional+'Global',
                    report=(f'{{{debug}?("~ Clean optional globally"):""}}')),
        ReportTrait(cleanUnits+'Global',
                    report=(f'{{{debug}?("~ Clean units globally"):""}}')),
        ReportTrait(fixOptional+'Control',
                    report=(f'{{{debug}?("~ Fix opt Control "+'
                            f'{optControl}):""}}')),
        ReportTrait(effTurn,
                    report=(f'{{{debug}?("~Set effective turn "+'
                            f'{globalTurn}+" ("+'
                            f'Turn+" "+EffTurn+" "+{globalScenario}+")"):""}}')),
        BasicTrait()]
    prototypeContainer.addPrototype(name        = f'Setup prototype',
                                    description = f'Setup prototype',
                                    traits      = traits)
    # ----------------------------------------------------------------
    #
    # VP prototype
    #
    traits = [
        GlobalPropertyTrait(
            ['',updateVP+'ZeroCS',GlobalPropertyTrait.DIRECT, f'{{0}}'],
            name = f'{globalVP}Confederate',
            numeric = True),
        GlobalPropertyTrait(
            ['',updateVP+'ZeroUS',GlobalPropertyTrait.DIRECT, f'{{0}}'],
            name = f'{globalVP}Union',
            numeric = True),
        GlobalHotkeyTrait(name         = '',
                          key          = updateVP+'Global',
                          globalHotkey = updateVP+'Trampoline'),
        TriggerTrait(name       = 'Update VPs',
                     command    = '',
                     key        = updateVP+'Turn',
                     actionKeys = [updateVP+'ZeroUS',
                                   updateVP+'ZeroCS',
                                   updateVP+'Global']),
        TriggerTrait(name       = 'CheckVPs 8',
                     command    = '',
                     key        = checkVP+'8',
                     property   = '',# Hmm, no logic here 
                     actionKeys = []),
        TriggerTrait(name       = 'CheckVPs 16',
                     command    = '',
                     key        = checkVP+'16',
                     property   = '',# Hmm no logic here 
                     actionKeys = []),
        TriggerTrait(name       = 'CheckVPs 24',
                     command    = '',
                     key        = checkVP+'24',
                     actionKeys = []),
        TriggerTrait(name       = 'CheckVPs',
                     command    = '',
                     key        = checkVP,
                     property   = '{{EffectiveTurn<=9}}',
                     actionKeys = [updateVP+'Turn',checkVP+'8']),
        TriggerTrait(name       = 'CheckVPs',
                     command    = '',
                     key        = checkVP,
                     property   = '{{EffectiveTurn>9&&EffectiveTurn<=17}}',
                     actionKeys = [updateVP+'Turn',checkVP+'16']),
        TriggerTrait(name       = 'CheckVPs',
                     command    = '',
                     key        = checkVP,
                     property   = '{{EffectiveTurn>17}}',
                     actionKeys = [updateVP+'Turn',checkVP+'24']),
        ReportTrait(updateVP+'ZeroCS',updateVP+'ZeroUS',
                    report = (f'{{{debug}?("Zero VPs: "+'
                              f'{globalVP}Confederate+" "+'
                              f'{globalVP}Union):""}}')),
        ReportTrait(updateVP+'Global',
                    report = (f'{{"`VPs: "+'
                              f'"Confedarate="+{globalVP}Confederate+" "+'
                              f'"Union="+{globalVP}Union}}')),
        ReportTrait(checkVP+'8',
                    report = (f'{{{verbose}?('
                              f'(({globalVP}Union>0)&&'
                              f'{globalVP}Confederate>='
                              f'(2*{globalVP}Union))?'
                              f'Alert('
                              f'"Confederate faction wins sudden death"'
                              f'+" "+{globalVP}Confederate+" >= 2 x "'
                              f'+{globalVP}Union+" = "'
                              f'+(2*{globalVP}Union)):"Nothing"):""}}')),
        ReportTrait(checkVP+'16',
                    report = (f'{{{verbose}?('
                              f'({globalVP}Confederate>='
                              f'(15+{globalVP}Union))?'
                              f'Alert('
                              f'"Confederate faction wins sudden death"'
                              f'+" "+{globalVP}Confederate+" >= 15 + "'
                              f'+{globalVP}Union+" = "'
                              f'+({globalVP}Union+15)):"Nothing"):""}}')),
        ReportTrait(checkVP+'24',
                    report = (f'{{{verbose}?Alert('
                              f'(({globalVP}Confederate>{globalVP}Union)?'
                              f'"Confederate":'
                              f'(({globalVP}Union>{globalVP}Confederate)?'
                              f'"Union":"No"))+" faction wins"):""}}')),
        BasicTrait(),
    ]
    prototypeContainer.addPrototype(name        = f'VP prototype',
                                    description = f'VP prototype',
                                    traits      = traits)
    
                    
    # ----------------------------------------------------------------
    # Create prototype to update defender hexes (at most 6)
    board = main['mapName']
    traits = [
        CalculatedTrait(
            name        = 'InWoods',
            expression  = (f'{{{globalWoods}.contains(":"+'
                           f'LocationName+":")}}'),
            description = 'Defender in woods hex?'),
        CalculatedTrait(
            name        = 'InHill',
            expression  = (f'{{({globalHill}.contains(":"+'
                           f'LocationName+":")||'
                           f'{globalRough}.contains(":"+'
                           f'LocationName+":"))}}'),
            description = 'Defender in hill hex?'),
        CalculatedTrait(
            name        = 'InTown',
            expression  = (f'{{{globalTown}.contains(":"+'
                           f'LocationName+":")}}'),
            description = 'Defender in town hex?'),
        TriggerTrait(
            name       = '',
            key        = updateHex,
            actionKeys = [f'{updateHex}{no}Defender' for no in range(1,7)],
            property   = '{!IsAttacker}'),
        TriggerTrait(
            name       = '',
            key        = updateHex,
            actionKeys = [f'{updateHex}{no}Attacker' for no in range(1,7)],
            property   = '{IsAttacker}'),
        ReportTrait(
            updateHex,
            report = (f'{{{debug}?("~"+BasicName+" "+LocationName'
                      f'+" In woods: "+InWoods'
                      f'+" In Hill: "+InHill'
                      f'+" In Town: "+InTown'
                      f'+" Woods: "+{repWoods}'
                      f'+" Hill: " +{repHill}'
                      f'+" Town: " +{repTown}'
                      f'+""):""}}'))
    ]
    edgwoods = []
    edghill  = []
    defwoods = []
    defhill  = []
    deftown  = []
    attwoods = []
    atthill  = []
    for no in range(1,7):
        notthis = '&&'.join([f'({defenderHex}{oth}!=LocationName)'
                             for oth in range(1,7) if oth != no])
        thisEmpty = f'{defenderHex}{no}==""'
        notRemote = 'true' # this should be set some other way
        notRemote = '(DoubleRange!=true||!IsRemote)'
        edgwoods.append(
            CalculatedTrait(
                name        = f'WoodsEdge{no}',
                expression  = (f'{{{globalWoodsEdges}.contains(":"+'
                               f'LocationName+{defenderWoods}{no}+":")}}'),
                description = f''))
        edghill.append(
            CalculatedTrait(
                name        = f'HillEdge{no}',
                expression  = (f'{{{globalHillEdges}.contains(":"+'
                               f'LocationName+{defenderHill}{no}+":")}}'),
                description = f''))
        defwoods.append(
            GlobalPropertyTrait(
                ['',f'{updateHex}{no}Defender',GlobalPropertyTrait.DIRECT,
                 f'{{((InWoods&&{thisEmpty.replace("Hex","Woods")})&&'
                 f'{notthis.replace("Hex","Woods")}&&{notRemote})?LocationName:'
                 f'{defenderWoods}{no}}}'],
                ['',f'{updateHex}{no}Attacker',GlobalPropertyTrait.DIRECT,
                 f'{{(InWoods&&WoodsEdge{no})?"":{defenderWoods}{no}}}'],
                name        = f'{defenderWoods}{no}',
                numeric     = True,
                description = 'Update defender woods to this unit'))
        defhill.append(
            GlobalPropertyTrait(
                ['',f'{updateHex}{no}Defender',GlobalPropertyTrait.DIRECT,
                 f'{{((InHill&&{thisEmpty.replace("Hex","Hill")})&&'
                 f'{notthis.replace("Hex","Hill")}&&{notRemote})?LocationName:'
                 f'{defenderHill}{no}}}'],
                ['',f'{updateHex}{no}Attacker',GlobalPropertyTrait.DIRECT,
                 f'{{(InHill&&HillEdge{no})?"":{defenderHill}{no}}}'],
                name        = f'{defenderHill}{no}',
                numeric     = True,
                description = 'Update defender hill to this unit'))
        deftown.append(
            GlobalPropertyTrait(
                ['',f'{updateHex}{no}Defender',GlobalPropertyTrait.DIRECT,
                 f'{{((InTown&&{thisEmpty.replace("Hex","Town")})&&'
                 f'{notthis.replace("Hex","Town")}&&{notRemote})?LocationName:'
                 f'{defenderTown}{no}}}'],
                name        = f'{defenderTown}{no}',
                numeric     = True,
                description = 'Update defender hill to this unit'))
    
    traits.extend(edgwoods+edghill+defwoods+defhill+deftown+[BasicTrait()])
    prototypeContainer.addPrototype(name        = f'Defender hex prototype',
                                    description = f'Defender hex prototype',
                                    traits      = traits)


    # ----------------------------------------------------------------
    # Create terrain prototype
    board    = main['mapName']
    traits = [BasicTrait()]
    prototypeContainer.addPrototype(name        = f'Terrain prototype',
                                    description = f'Terrain prototype',
                                    traits      = traits)

    repAttackHill  = '+","+'.join([f'AttackHill{no}'  for no in range(1,7)])
    repAttackWoods = '+","+'.join([f'AttackWoods{no}' for no in range(1,7)])
    repAttackTown  = '+","+'.join([f'AttackTown{no}'  for no in range(1,7)])
    
    
    # ----------------------------------------------------------------
    # Battle unit prototype update with forest, fortress, overriver rules
    #
    # Need a bit more massage
    print('Battle unit')
    battleUnitP = prototypes[battleUnit]
    traits      = battleUnitP.getTraits()
    basic       = traits.pop()
    effAF       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='EffectiveAF')
    effDF       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='EffectiveDF')
    oddsS       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='OddsShift')
    calAF       = Trait.findTrait(traits,GlobalPropertyTrait.ID,
                                  key='name', value=battleAF)
    calDF       = Trait.findTrait(traits,GlobalPropertyTrait.ID,
                                  key='name', value=battleDF)
    calAFCmds   = calAF.getCommands()
    calAFCmd    = calAFCmds[0]
    calAFkey    = calAFCmd[1]
    calAFCmd[1] = calAFkey+'Real'
    calDFCmds   = calDF.getCommands()
    calDFCmd    = calDFCmds[0]
    calDFkey    = calDFCmd[1]
    calDFCmd[1] = calDFkey+'Real'
    calAF.setCommands(calAFCmds)
    calDF.setCommands(calDFCmds)
    print(' Calculate AF commands',calAFCmds)
    print(' Calculate DF commands',calDFCmds)
    # print(oddsS.getCommands())
    if effAF: traits.remove(effAF)
    if effDF: traits.remove(effDF)
    # if oddsS: traits.remove(oddsS)

    toremove = []
    for trait in traits:
        if trait.ID != ReportTrait.ID: continue
        # print(trait['report'])
        if 'to total attack factor' in trait['report']:
            toremove.append(trait)
        if 'to total defence factor' in trait['report']:
            toremove.append(trait)
    for trait in toremove:
        # print('Remove',trait['report'])
        traits.remove(trait)


    repWoodsEdges = '+","+'.join([f'WoodsEdge{no}' for no in range(1,7)])
    repHillEdges  = '+","+'.join([f'HillEdge{no}'  for no in range(1,7)])
    printKey = key(NONE,0)+',print'
    traits.extend([
        PrototypeTrait(name='Defender hex prototype'),
        CalculatedTrait(
            name        = 'EffectiveAF',
            expression  = f'{{({arty}&&!{uarty})?0:(CF+BonusAF)}}',
            description = 'Calculate effective CF'),
        CalculatedTrait(
            name        = 'EffectiveDF',
            expression  = (f'{{({arty}&&!{uarty})?0:(DF+BonusDF)}}'),
            description = 'Calculate effective CF'),
        TriggerTrait(
            name       = '',
            key        = calAFkey,
            actionKeys = [calAFCmd[1]]),
        TriggerTrait(
            name       = '',
            key        = calDFkey,
            actionKeys = [calDFCmd[1]]),
        TriggerTrait(
            name       = 'Print',
            command    = 'Print',
            key        = printKey,
            property   = f'{{{debug}}}',
            actionKeys = []),
        GlobalPropertyTrait(['',calAFCmd[1],GlobalPropertyTrait.DIRECT,
                             f'{{{globalNA}+({arty}&&!{uarty}?0:1)}}'],
                            name = globalNA,
                            numeric = True,
                            description=('Increment number of attackers')),
        GlobalPropertyTrait(['',calDFCmd[1],GlobalPropertyTrait.DIRECT,
                             f'{{{globalND}+({arty}&&!{uarty}?0:1)}}'],
                            name = globalND,
                            numeric = True,
                            description=('Increment number of defenders')),
        ReportTrait(printKey,
                    report=(f'{{BasicName+":"'
                            f'+" Hex="+LocationName+" ("'
                            f'+" Woods="+InWoods'
                            f'+" Hill="+InHill'
                            f'+" Town="+InTown+")"'
                            f'+" AF="+EffectiveAF'
                            f'+" DF="+EffectiveDF'
                            f'+" Faction="+Faction'
                            f'+" Attacker="+IsAttacker'
                            f'+" Woods: "+{repWoods}'
                            f'+" Hill:  "+{repHill}'
                            f'+" Town:  "+{repTown}'
                            f'+" Woods edge="+{repWoodsEdges}'
                            f'+" Hill edge="+{repHillEdges}'
                            #f'+" Wood edges: "+{globalWoodsEdges}' 
                            #f'+" Hill edges: "+{globalHillEdges}'
                            f'+" Step="+Step_Level'
                            f'+" Rest="+Rest_Level'
                            f'+" VP="+VP'
                            f'}}')),
        ReportTrait(calAFCmd[1],
                    report = (f'{{{debug}?("! "+BasicName'
                              f'+" Hex="+LocationName+" ("'
                              f'+" Woods="+InWoods'
                              f'+" Hill="+InHill'
                              f'+" Town="+InTown+")"'
                              f'+" AF="+CF+"+"+BonusAF+" ("'
                              f'+" Woods: "+{repWoods}'
                              f'+" Hill:  "+{repHill}'
                              f'+" Town:  "+{repTown}'
                              f'+" Woods edge="+{repWoodsEdges}'
                              f'+" Hill edge="+{repHillEdges}'
                              #f'+" Wood edges: "+{globalWoodsEdges}' 
                              #f'+" Hill edges: "+{globalHillEdges}'
                              f'+")="+EffectiveAF+"=>"'
                              f'+{battleAF}+" # A "'
                              f'+{globalND}+" "'
                              f'+{arty}+" Phase="+Phase'
                              f'+" is arty="+{uarty}+" "+Type'
                              f'):""}}')),
        ReportTrait(calDFCmd[1],
                    report = (f'{{{debug}?("! "+BasicName'
                              f'+" Hex="+LocationName+" ("'
                              f'+" Woods="+InWoods'
                              f'+" Hill="+InHill'
                              f'+" Town="+InTown+")"'
                              f'+" DF="+DF+"+"+BonusDF+" ("'
                              f'+" Woods: "+{repWoods}'
                              f'+" Hill:  "+{repHill}'
                              f'+" Town:  "+{repTown}'
                              f'+" Woods edge="+{repWoodsEdges}'
                              f'+" Hill edge="+{repHillEdges}'
                              #f'+" Wood edges: "+{globalWoodsEdges}' 
                              #f'+" Hill edges: "+{globalHillEdges}'
                              f'+")="+EffectiveDF+"=>"'
                              f'+{battleDF}+" # D "'
                              f'+{globalND}+" "'
                              f'+{arty}+" Phase="+Phase'
                              f'+" is arty="+{uarty}+" "+Type'
                              f'):""}}')),
        ReportTrait(updateHex,
                    report = (f'{{{debug}?("! "+BasicName+" in "'
                              f'+LocationName+" update hex -> "'
                              f'+" Attacker: "+IsAttacker'
                              f'+" In woods: "+InWoods'
                              f'+" In Hill: "+InHill'
                              f'+" In Town: "+InTown'
                              f'+" Woods: "+{repWoods}'
                              f'+" Hill:  "+{repHill}'
                              f'+" Town:  "+{repTown}'
                              f'+" Woods edge="+{repWoodsEdges}'
                              f'+" Hill edge="+{repHillEdges}'
                              #f'+" Wood edges: "+{globalWoodsEdges}' 
                              #f'+" Hill edges: "+{globalHillEdges}'
                              f'):""}}')),
        basic
        ])
    rest = RestrictCommandsTrait(
        keys          = [declareKey],
        name          = 'Disable when not in combat',
        hideOrDisable = RestrictCommandsTrait.DISABLE,
        expression    = f'{{{globalDeclare}==true}}')
    battleUnitP.setTraits(rest,*traits)

    # ----------------------------------------------------------------
    # Battle calculation update with forest, fortress, overriver rules
    #
    #
    #
    limitAF     = key(NONE,0)+',limitAF'
    limitDF     = key(NONE,0)+',limitDF'
    addWoods    = key(NONE,0)+',addWoods'
    addHill     = key(NONE,0)+',addHill'
    addTown     = key(NONE,0)+',addTown'
    battleCalcP = prototypes[battleCalc]
    traits      = battleCalcP.getTraits()
    basic       = traits.pop()
    calcOdds    = Trait.findTrait(traits,TriggerTrait.ID,
                                  key   = 'key',         
                                  value = calcOddsKey)
    calcFrac    = Trait.findTrait(traits,TriggerTrait.ID,
                                  key   = 'key',         
                                  value = calcFracKey)
    batFrac    = Trait.findTrait(traits,CalculatedTrait.ID,
                                 key   = 'name',         
                                 value = 'BattleFraction')
    oddsIdx    = Trait.findTrait(traits,CalculatedTrait.ID,
                                 key   = 'name',         
                                 value = 'OddsIndex')
    oldFrac    = batFrac['expression'][1:-1]
    oldIx      = oddsIdx['expression'][1:-1]
    oddsIdx['expression'] = f'{{({globalND}<1||{globalNA}<1)?0:({oldIx})}}'

    resetHex = [
        GlobalPropertyTrait(
            ['',updateHex+f'Reset{no}',GlobalPropertyTrait.DIRECT,
             f'{{""}}'],
            name        = f'{defenderWoods}{no}',
            numeric     = False,
            description = 'Reset defender woods hex')
        for no in range(1,7)]
    resetHex += [
        GlobalPropertyTrait(
            ['',updateHex+f'Reset{no}',GlobalPropertyTrait.DIRECT,
             f'{{""}}'],
            name        = f'{defenderHill}{no}',
            numeric     = False,
            description = 'Reset defender hill hex')
        for no in range(1,7)]
    resetHex += [
        GlobalPropertyTrait(
            ['',updateHex+f'Reset{no}',GlobalPropertyTrait.DIRECT,
             f'{{""}}'],
            name        = f'{defenderTown}{no}',
            numeric     = False,
            description = 'Reset defender town hex')
        for no in range(1,7)]
        
    resetHexes = [updateHex+f'Reset{no}' for no in range(1,7)]
    traits.extend(resetHex+
                  [ReportTrait(
                      *resetHexes,
                      report=(f'{{{debug}?("! Reset defender hexes"+'
                              f'"Woods: "+{repWoods}+" "+'
                              f'"Hill:  "+{repHill}+" "+'
                              f'"Town:  "+{repTown}):""}}'))])
    traits.extend([
        # --- Set not combat phase ----------------------------------
        GlobalPropertyTrait(
            ['',checkDeclare,GlobalPropertyTrait.DIRECT,
             f'{{Phase!="{usCombat}"&&'
             f'Phase!="{usArt}"&&'
             f'Phase!="{usMove}"&&'
             f'Phase!="{csCombat}"&&'
             f'Phase!="{csArt}"&&'
             f'Phase!="{csMove}"}}'],
            name = globalDeclare,
            numeric = True,
            description = 'Check for (any) declare phase'),
        # --- Set not combat phase ----------------------------------
        GlobalPropertyTrait(
            ['',checkResolve,GlobalPropertyTrait.DIRECT,
             f'{{!Phase.contains("combat")&&!Phase.contains("artillery")}}'],
            name        = globalResolve,
            numeric     = True,
            description = 'Check for (any) resolve phase'),
        # --- Set not setup phase -----------------------------------
        GlobalPropertyTrait(
            ['',checkSetup,GlobalPropertyTrait.DIRECT,
             f'{{Phase!="Setup"||Turn!=1}}'],
            name        = globalSetup,
            numeric     = True,
            description = 'Check for setup phase'),
        # --- Update hex of defenders --------------------------------
        GlobalHotkeyTrait(
            name         = '',
            key          = optKey,
            globalHotkey = optKey,
            description  = 'Show optional rules'),
        # --- Update hex of defenders --------------------------------
        GlobalHotkeyTrait(
            name         = '',
            key          = updateHex+'Real',
            globalHotkey = updateHex,
            description  = 'Ask GKC to update defender hex'),
        # --- Limit total AF -----------------------------------------
        GlobalPropertyTrait(
            ['',limitAF,GlobalPropertyTrait.DIRECT,
             f'{{{battleAF}>10?10:({battleAF}<0?0:{battleAF})}}'],
            name        = battleAF,
            numeric     = True,
            description = 'Limit AF to from 0 to 10'),
        # --- Limit total DF -----------------------------------------
        # Before the this, we must add possible terrain modifiers
        # Count the number of non-empty string of 'defenderWoods' and
        # 'defenderHill' and 'defenderTown' (only one).
        GlobalPropertyTrait(
            ['',limitDF,GlobalPropertyTrait.DIRECT,
             f'{{{battleDF}>10?10:({battleDF}<0?0:{battleDF})}}'],
            ['',addWoods,GlobalPropertyTrait.DIRECT,
             f'{{{battleDF}+'+'+'.join([f'({defenderWoods}{no}!=""?1:0)'
                                       for no in range(1,7)])+f'}}'],
            
            ['',addHill,GlobalPropertyTrait.DIRECT,
             f'{{{battleDF}+'
             # Do not add hill DF bonus during artillery bombardment 
             #f'(Phase.contains("artillery")?0:1)*'
             f'('+
             '+'.join([f'({defenderHill}{no}!=""?2:0)'
                       for no in range(1,7)])+f')}}'],
            ['',addTown,GlobalPropertyTrait.DIRECT,
             f'{{{battleDF}+'+'+'.join([f'({defenderTown}{no}!=""?1:0)'
                                        for no in range(1,7)])+f'}}'],
            name        = battleDF,
            numeric     = True,
            description = 'Limit DF to from 0 to 10'),
        # --- Zero battle counts --------------------------------------
        GlobalPropertyTrait(
            ['',key(NONE,0)+',wgZeroBattleDF',
             GlobalPropertyTrait.DIRECT,'{0}'],
            name        = globalND,
            numeric     = True,
            description = 'Zero number of defenders'),
        GlobalPropertyTrait(
            ['',key(NONE,0)+',wgZeroBattleAF',
             GlobalPropertyTrait.DIRECT,'{0}'],
            name        = globalNA,
            numeric     = True,
            description = 'Zero number of attackers'),
        # --- Update defender hexes -----------------------------------
        TriggerTrait(
            name       = '',
            key        = updateHex,
            actionKeys = resetHexes+[updateHex+'Real']),
        # --- Reports ------------------------------------------------
        ReportTrait(checkDeclare,
                    report = (f'{{{debug}?("~ Check for declare phase "+'
                              f'Phase+" -> {globalDeclare}="+'
                              f'{globalDeclare}):""}}')),
        ReportTrait(checkResolve,
                    report = (f'{{{debug}?("~ Check for resolve phase "+'
                              f'Phase+" -> {globalResolve}-"+'
                              f'{globalResolve}):""}}')),
        ReportTrait(checkSetup,
                    report = (f'{{{debug}?("~ Check for setup phase "+'
                              f'Phase+" -> {globalSetup}-"+'
                              f'{globalSetup}):""}}')),
        ReportTrait(updateHex,
                    report = (f'{{{debug}?("~ "+BasicName+" Update def hex "+'
                              f'"Woods "+{repWoods}+" "+'
                              f'"Hill "+{repHill}+" "+'
                              f'"Town "+{repTown}):""}}')),
        ReportTrait(limitAF,
                    report = (f'{{{debug}?("~ "+BasicName+" Limit AF "+'
                              f'{battleAF}):""}}')),
        ReportTrait(limitDF,
                    report = (f'{{{debug}?("~ "+BasicName+" Limit DF "+'
                              f'{battleDF}):""}}')),
        ReportTrait(calcIdxKey,
                    report = (f'{{{debug}?("~ "+BasicName+" Total DF "+'
                              f'{battleDF}+" (# "+{globalND}+") AF "+'
                              f'{battleAF}+" (# "+{globalNA}+")")'
                              f':""}}')),
            
        basic
    ])
    keys = calcFrac.getActionKeys()
    calcFrac.setActionKeys(keys[:-1]+
                           [addWoods,addHill,addTown]+
                           [limitAF,limitDF]+keys[-1:])
    keys = calcOdds.getActionKeys()
    calcOdds.setActionKeys([updateHex]+keys)
    battleCalcP.setTraits(*traits)
        
    # ----------------------------------------------------------------
    # Modify faction prototypes 
    for faction in ['Confederate', 'Union']:
        print(f'Faction {faction} prototype')
        factionp = prototypes[f'{faction} prototype']
        ftraits  = factionp.getTraits()
        fbasic   = ftraits.pop()
        opponent = 'Union' if faction == 'Confederate' else 'Confederate'
        
        # Remove traits we don't want nor need
        fdel     = Trait.findTrait(ftraits,DeleteTrait.ID)
        fdel['key'] = deleteKey
        fdel['name'] = ''

        felim        = Trait.findTrait(ftraits,SendtoTrait.ID,
                                       'key', eliminateKey)
        felim['key'] = eliminateCmd
        felim['name'] = ''

        ftraits.append(
            MarkTrait(
                name       = pieceLayer,
                value      = unitLayer))

        hqtraits = ftraits.copy()
        hqtraits.remove(felim)
        
        ftraits.extend([
            MarkTrait(name='HasVP',value='true'),
            CalculatedTrait(
                name       = 'VP',
                expression = ('{CurrentMap=="DeadMap"?CF:'
                              '(Step_Level==2?1:0)}')),
            GlobalPropertyTrait(
                ['',updateVP,GlobalPropertyTrait.DIRECT,
                 f'{{{globalVP}{opponent}+VP}}'],
                name = f'{globalVP}{opponent}',
                numeric = True),
            TriggerTrait(
                name       = 'Step loss',
                command    = 'Step loss',
                key        = stepKey,
                actionKeys = [stepOrElim]),
            TriggerTrait(
                name       = '',
                command    = '',
                key        = stepOrElim,
                actionKeys = [stepCmd],
                property   = '{CanFlip&&Step_Level==1&&CurrentMap!="DeadMap"}'),
            TriggerTrait(
                name       = '',
                command    = '',
                key        = stepOrElim,
                actionKeys = [eliminateCmd+'Step'],
                property   = '{(CanFlip&&Step_Level==2)||!CanFlip}'),
            TriggerTrait(
                name       = 'Do eliminate and reset step',
                command    = '',
                key        = eliminateCmd+'Step',
                actionKeys = [eliminateCmd,resetStep]),
            TriggerTrait(
                name       = '',
                command    = 'Eliminate',
                key        = eliminateKey,
                actionKeys = [eliminateCmd]),
            ReportTrait(
                updateVP,
                report = (f'{{({debug}||({verbose}&&VP>0))?'
                          f'("!"+BasicName+" Update VPs "+VP+'
                          f'" -> "+{globalVP}{opponent}):""}}')),
            ReportTrait(
                stepKey,
                report = (f'{{{verbose}?("~"+BasicName+" Step loss or eliminate "'
                          f'+Step_Level+" "+CurrentMap+" "+LocationName):""}}')),
            ReportTrait(
                stepCmd,
                report = (f'{{{verbose}?("~"+BasicName+" Step loss "+'
                          f'Step_Level+" "+CurrentMap+" "+LocationName):""}}')),
            ReportTrait(
                eliminateCmd,
                report = (f'{{{verbose}?("~"+BasicName+" Eliminate "+'
                          f'Step_Level+" "+CurrentMap+" "+LocationName):""}}')),
            ReportTrait(
                eliminateCmd+'Step',
                report = (f'{{{verbose}?("~"+BasicName+" Eliminate & Step "+'
                          f'Step_Level+" "+CurrentMap+" "+LocationName):""}}')),
            ReportTrait(
                deleteKey,
                report = (f'{{{debug}?("~ Deleting "+BasicName+" @ "'
                          f'+LocationName+" "+CurrentBoard):""}}'))])
        
        # ftraits.remove(fdel)
        factionp.setTraits(*ftraits,fbasic)

        btlp = Trait.findTrait(hqtraits,PrototypeTrait.ID,
                               'name','wgBattleUnit')
        if btlp:
            print(' Remove battle unit prototype from faction HQ prototype')
            hqtraits.remove(btlp)

        hqtraits.append(BasicTrait())
        
        prototypeContainer\
            .addPrototype(name        = f'{faction} HQ prototype',
                          description = f'{faction} HQ prototype',
                          traits      = hqtraits)

    # ----------------------------------------------------------------
    # Modify faction prototypes
    remoteOff = key(NONE,0)+',wgClearBattle'
    for utype in ['artillery', 'artillery reconnaissance']:
        utypep   = prototypes[f'{utype} prototype']
        ftraits  = utypep.getTraits()
        fbasic   = ftraits.pop()
        ftraits.extend([
            MarkTrait(name='DoubleRange',value=True),
            DynamicPropertyTrait(
                ['',remoteOn,DynamicPropertyTrait.DIRECT,'{2}'],
                ['',remoteOff,DynamicPropertyTrait.DIRECT,'{1}'],
                name    = 'RemoteOn',
                numeric = True,
                value   = '{1}'),
            CalculatedTrait(
                name       = 'IsRemote',
                expression = '{RemoteOn==2}'),
            LayerTrait(
                name = 'Remote',
                images = ['', 'remote.png'],
                newNames = ['+','Remote fire +'],
                activateName = '',
                activateKey  = '',
                increaseName = '',
                increaseKey  = '',
                resetName    = '',
                resetKey     = '',
                loop         = False,
                always       = True,
                follow       = True,
                expression   = '{RemoteOn}'
            ),
            TriggerTrait(
                name         = 'Signal remote fire',
                command      = 'Indirect',
                key          = key('I',CTRL),
                actionKeys   = [remoteOn],
                property     = '{RemoteOn==1&&!IsAttacker&&Phase.contains("combat")}'),
            RestrictCommandsTrait(
                keys          = [key('I',CTRL)],
                name          = 'Restrict remote based on optional',
                hideOrDisable = RestrictCommandsTrait.HIDE,
                expression    = f'{{!{optIndirect}}}'),
        ])
        utypep.setTraits(*ftraits,fbasic)

    # ----------------------------------------------------------------
    #
    # Get all pieces and modify them
    #
    pieces  = game.getPieces(asdict=False)
    mxTurns = {1: 1, 2: 9, 3: 17, 0: 0}

    from data import setups

    print('Loopinvg over all pieces')
    for piece in pieces:
        name    = piece['entryName'].strip()
        faction = name[:3]
        Faction = 'Confederate' if faction == 'cs ' else 'Union'
        parent  = piece.getParent()
        oob     = None
        scen    = 0

        if parent and parent.TAG == AtStart.TAG:
            pboard = parent.getParent()
            print(f' {name:20s} at {parent["name"]:30s} in {pboard["mapName"]}')
            oob = pboard
            if 'OOB Jul' in pboard['mapName']:
                scen = int(pboard['mapName'][-1])

        stKey = (start1Key if scen == 1 else
                 start2Key if scen == 2 else
                 start3Key if scen == 3 else '')
        mxTurn = mxTurns.get(scen,0)

            
        if name == 'scenario 1':
            # Modify scenario selector piece
            traits               = piece.getTraits()
            basic                = traits.pop()
            step                 = Trait.findTrait(traits, LayerTrait.ID,
                                                   'name', 'Step')
            bky                  = key(NONE,0)+',scenario'
            imgs                 = step['images'].split(',')
            step['resetKey']     = ''
            step['resetName']    = ''
            step['increaseKey']  = ''
            step['increaseName'] = ''
            step['follow']       = True
            step['expression']   = f'{{{globalScenario}}}'
            step['images']       = ','.join([imgs[0],
                                             imgs[0].replace("1","2"),
                                             imgs[0].replace("1","3")])
            #print(f' images: {step["images"]}')
            step['newNames']    = 'Jul 1,Jul 2,Jul 3'
            traits = [
                ClickTrait(
                    key         = '',
                    context     = True,
                    whole       = True,
                    description = 'Select start-up'),
                RestrictCommandsTrait(
                    name          = 'Restrict scenario choice',
                    hideOrDisable = RestrictCommandsTrait.DISABLE,
                    expression    = f'{{Phase!="Setup"}}',
                    keys          = [bky+'1',bky+'2',bky+'3']),
                GlobalPropertyTrait(
                    ['Jul 1',bky+'1',GlobalPropertyTrait.DIRECT, f'{{1}}'],
                    ['Jul 2',bky+'2',GlobalPropertyTrait.DIRECT, f'{{2}}'],
                    ['Jul 3',bky+'3',GlobalPropertyTrait.DIRECT, f'{{3}}'],
                    name        = globalScenario,
                    numeric     = True,
                    wrap        = True,
                    description = 'Select scenario'),
                RestrictAccessTrait(sides=[],
                                    description='Cannot move'),
                ReportTrait(bky+'1',bky+'2',bky+'3',
                            report=(f'{{"Set-up turn: "+{globalScenario}}}')),
                step,
                basic]
            piece.setTraits(*traits)
            
        elif name == 'initiative':
            # Initiative marker 
            traits = piece.getTraits()
            basic  = traits.pop()
            step   = Trait.findTrait(traits, LayerTrait.ID, 'name', 'Step')
            useKy  = key(NONE,0)+',useInitiative'
            mvKy   = key(NONE,0)+',moveInitiative'
            step['newNames']     = 'Confederate +,Union +'
            step['increaseName'] = ''
            step['increaseKey']  = useKy;
            traits.remove(step)

            reg = ('{Step_Level==1?'
                   '"us initiative":"cs initiative"}')
            ufac = ('(Step_Level==1?'
                    '"Confederate":"Union")')
            traits.extend([
                RestrictCommandsTrait(
                    name          = 'Restrict initiative to combat',
                    hideOrDisable = RestrictCommandsTrait.DISABLE,
                    expression    = (f'{{{globalResolve}==true}}'),
                    keys          = [stepKey]),
                SendtoTrait(
                    name        = '',
                    key         = mvKy,
                    mapName     = 'Board',
                    boardName   = 'Board',
                    destination = 'R',
                    region      = reg,
                    restoreName = '',
                    restoreKey  = '',
                    position    = ''),
                step,
                TriggerTrait(
                    name       = 'Use',
                    command    = 'Use',
                    key        = stepKey,
                    actionKeys = [mvKy,useKy]),
                ReportTrait(
                    stepKey,
                    report = (f'{{{verbose}?("!"+{ufac}+'
                              f'" uses the initiative chit"):""}}')),
            ])
                
            if oob is not None:
                if 'Confederate' in oob['mapName']: step['level'] = 1
                    
            prot   = Trait.findTrait(traits,PrototypeTrait.ID,
                                     'name', 'Markers prototype')
            if prot: traits.remove(prot)
            prot   = Trait.findTrait(traits,PrototypeTrait.ID,
                                     'name', ' prototype')
            if prot: traits.remove(prot)

            if oob is not None:
                traits.append(
                    SendtoTrait(name        = '',
                                key         = stKey,
                                mapName     = 'Board',
                                boardName   = 'Board',
                                destination = 'R',
                                region      = 'cs initiative',
                                restoreName = '',
                                restoreKey  = '',
                                position    = 'cs initiative'))
            if oob is not None:
                traits.append(MarkTrait(name="UnitScenario",
                                        value=scen))
                
            piece.setTraits(*traits,basic)
            
        elif name.startswith('opt '):
            # Options marker 
            optName = name.replace('opt ','').strip()
            gpName  = name.replace(' ','')
            
            traits = piece.getTraits()
            basic  = traits.pop()
            step   = Trait.findTrait(traits,LayerTrait.ID,
                                     key = 'name',
                                     value = 'Step')
            step['newNames']     = ['+ enabled','+ disabled']
            step['increaseName'] = ''
            step['increaseKey']  = ''
            step['follow']       = True
            step['expression']   = f'{{{gpName}==true?1:2}}'
            traits.extend([
                MarkTrait(name='OptName',value=optName),
                GlobalPropertyTrait(
                    ["",setOptional,GlobalPropertyTrait.DIRECT,
                     f'{{!{gpName}}}'],
                    name = gpName,
                    numeric = True,
                    description = f'Toggle {gpName}'),
                basic
            ])
            piece.setTraits(*traits)
                
        elif name == hidden:
            traits = piece.getTraits()
            basic  = traits.pop()
            showN  = key(NONE,0)+',showNotes'
            initD  = key(NONE,0)+',initDice'
            traits = [
                TriggerTrait(name       = 'Show notes window for tutorial',
                             command    = '',
                             key        = tutorialKey,
                             property   = f'{{{isTutorial}==true}}',
                             actionKeys = [showN]),
                GlobalHotkeyTrait(name         = '',
                                  key          = showN,
                                  globalHotkey = key('N',ALT),
                                  description  = 'Show notes window'),
            ]+traits
            piece.setTraits(*traits,basic)

        elif name == gameTurn:
            # Game turn marker 
            gtraits  = piece.getTraits()
            markers  = Trait.findTrait(gtraits,
                                       PrototypeTrait.ID,
                                       key = 'name',
                                       value = 'Markers prototype')
            flip     = Trait.findTrait(gtraits,LayerTrait.ID)
            if markers is not None:
                gtraits.remove(markers)
            flip['increaseName'] = ''
            
            gbasic   = gtraits.pop()
            # Add the prototypes we created above 
            gtraits.extend([PrototypeTrait(pnn + ' prototype')
                            for pnn in turnp])
            gtraits.append(PrototypeTrait(name='Setup prototype'))
            gtraits.append(PrototypeTrait(name='VP prototype'))
            # Do not allow movement
            gtraits.append(RestrictAccessTrait(sides=[],
                                               description='Cannot move'))
            gtraits.append(gbasic)
            piece.setTraits(*gtraits)

            main.addAtStart(name            = 'Game turn',
                            location        = 'Turn 1@turns',
                            useGridLocation = True,
                            owningBoard     = main['mapName']).\
                            addPiece(piece)

        elif name == 'control':
            cps = {'F6': 5,
                   'I11': 3,
                   'K6': 1,
                   'K7': 1,
                   'L6': 1,
                   'L7': 1,
                   'M7': 1,
                   'M9': 3 }
            for vhex, vp in cps.items():
                cpy = main.addAtStart(name            = f'VP {vhex}',
                                      location        = vhex,
                                      useGridLocation = True,
                                      owningBoard     = main['mapName']).\
                                      addPiece(piece)
                traits = cpy.getTraits()
                basic  = traits.pop()
                traits.append(MarkTrait(name='VP',value=vp))
                cpy.setTraits(*traits,basic)
            
            
        elif faction in ['cs ','us ']:
            traits               = piece.getTraits()
            basic                = traits.pop()
            step                 = Trait.findTrait(traits, LayerTrait.ID,
                                                   'name', 'Step')
            ext                  = setups.get(scen,{}).\
               get(Faction,{}).\
               get(name,{})
            reducedCF            = Trait.findTrait(traits, MarkTrait.ID,
                                                   'name', 'ReducedCF')
            reorg                = None

            eprot                = Trait.findTrait(traits,
                                                   PrototypeTrait.ID,
                                                   'name',
                                                   ' prototype')
            if eprot is not None: traits.remove(eprot)
            arprot               = Trait.findTrait(traits,
                                                   PrototypeTrait.ID,
                                                   'name',
                                                   'artillery reconnaissance prototype')
            if arprot is not None:
                aprot = Trait.findTrait(traits,
                                        PrototypeTrait.ID,
                                        'name',
                                        'artillery prototype')
                rprot = Trait.findTrait(traits,
                                        PrototypeTrait.ID,
                                        'name',
                                        'reconnaissance prototype')
                #print(f' {name} is horse artillery {arprot} {aprot} {rprot}')
                if aprot is not None: traits.remove(aprot)
                if rprot is not None: traits.remove(rprot)
                
            
            if step is not None:
                traits.remove(step)
                step['loop']         = ''
                step['increaseKey']  = stepCmd
                step['increaseName'] = ''
                step['resetKey']     = resetStep
                reorg = [
                    RestrictCommandsTrait(
                        hideOrDisable = 'Disable',
                        expression    = (
                            f'{{'
                            f'!Phase.contains("{Faction} movement")||'
                            f'((EffectiveTurn%8)!=0)||'
                            f'!CanFlip||'
                            f'!{optNight}||'
                            f'Step_Level==1}}'),
                            keys       = [reorgKey]),
                    RestrictCommandsTrait(
                        hideOrDisable = 'Disable',
                        expression    = (
                            f'{{'
                            f'!{optNight}||'
                            f'Rest_Level==1}}'),
                            keys       = [reorgCmd+'Reset']),
                    LayerTrait(
                        name = 'Rest',
                        images = ['', 'reorganise.png'],
                        newNames = ['+','Reorganising +'],
                        activateName = '',
                        activateKey  = '',
                        increaseName = 'Reorganise',
                        increaseKey  = reorgKey,
                        resetName    = 'Stop reorganising',
                        resetKey     = reorgCmd+'Reset',
                        loop         = False,
                        always       = True
                    ),
                    ReportTrait(
                        reorgKey,
                        report = (f'{{{verbose}?("! Reorganising "+BasicName+'
                                  f'" "+Rest_Level):""}}')),
                    ReportTrait(
                        reorgKey,reorgCmd+'Reset',
                        report = (f'{{{verbose}?("! Reorganised "+BasicName+'
                                  f'" "+Rest_Level):""}}')),
                    ReportTrait(
                        resetStep,
                        report = (f'{{{verbose}?("! Reset "+BasicName+'
                                  f'" step loss "+Step_Level):""}}')),
                    TriggerTrait(
                        name       = 'Reorganised',
                        command    = '',
                        key        = reorgCmd+'Finish',
                        property   = '{Rest_Level==2&&Phase.contains(Faction)}',
                        actionKeys = [reorgCmd+'Reset',
                                      resetStep])]
                
            if reducedCF is None:
                #print(f' Removing step from {name}')
                step = None
            
            traits.append(MarkTrait(name='CanFlip',
                                    value=False if reducedCF is None else True))
            
            fl                   = ext.get('flipped',False)
            el                   = ext.get('elim',   False)
            hx                   = ext.get('start',None)
            tn                   = 100
            for trait in traits:
                if trait.ID == MarkTrait.ID:
                    if hx is None and trait['name'] == 'upper right':
                        st = trait['value']
                        st = sub('[^=]+=','',st).strip()
                        hx = st
                        if hx == '':
                            hx = None
                    if trait['name'] == 'upper left':
                        st  = trait['value']
                        st2 = sub('[^=]+=','',st).strip()
                        try:
                            tn = int(st2)
                        except:
                            tn = -1
            if oob is not None:
                ky      = stKey
                startup = None
                rest    = None
                rept    = None
                if  el:
                    startup  = SendtoTrait(name        = '',
                                           key         = ky,
                                           mapName     = 'DeadMap',
                                           boardName   = f'{Faction} pool',
                                           destination = 'L',
                                           restoreName = '',
                                           restoreKey  = '',
                                           description = 'Elimate on start')
                elif hx is not None and hx != '':
                    nm = ''
                    if tn < mxTurn:
                        rkey = ky
                    else:
                        nm   = 'Reinforce'
                        rkey = reinforceKey

                    dest    = 'G'
                    pos     = hx 
                    reg     = hx
                    startup = SendtoTrait(name        = nm,
                                          key         = rkey,
                                          mapName     = 'Board',
                                          boardName   = 'Board',
                                          destination = dest,
                                          region      = reg,
                                          restoreName = '',
                                          restoreKey  = '',
                                          position    = pos)
                    if tn >= mxTurn:
                        more = ''
                        if 'hq' in name:
                            # HQ.  If on oob and has been eliminated
                            # then only allow reinforcement when on
                            # night turns.
                            more = (f'||((((EffectiveTurn%8)!=0)&&Eliminated))')
                        rest = RestrictCommandsTrait(
                            'Restrict reinforce command',
                            hideOrDisable = 'Disable',
                            expression    = (
                                f'{{!Phase.contains("{Faction} movement")||'
                                f'(CurrentBoard=="Board")||'
                                f'(CurrentMap=="DeadMap")||'
                                f'(EffectiveTurn<{tn}){more}}}'),
                            keys       = [rkey])
                        rept = ReportTrait(
                            rkey,
                            report = (f'{{{verbose}?("!"+BasicName+'
                                      f'" reinforces at {reg}"):""}}'))
                        
                if fl and step != None: step['level'] = 2

                if rest:
                    traits.append(rest)
                if startup:
                    traits.append(startup)
                if rept:
                    traits.append(rept)
                traits.append(MarkTrait(name="UnitScenario",  value=scen))
    
            rest = RestrictCommandsTrait(
                'Restrict step command',
                hideOrDisable = 'Disable',
                expression    = (
                    f'{{!Phase.contains("combat")&&'
                    f'!Phase.contains("artillery")&&'
                    f'(Turn%8!=0)}}'),
                keys       = [stepKey])
            # traits.insert(0,rest)

            # Add step trait back in
            if step is not None: 
                if reorg is not None:
                    traits.extend(reorg)
                traits.append(step)

            # Modify basic prototype for SF units
            if ' hq' in name:
                #print(f' Modifying the HQ unit {name}')
                facp = Trait.findTrait(traits,PrototypeTrait.ID,
                                        'name', f'{Faction} prototype')
                if facp:
                    facp['name'] = f'{Faction} HQ prototype'
                    
                if oob is not None:
                    #print(f' Adding eliminte key to HQ {name} {oob["mapName"]} {parent}')
                    toOOB    = key(NONE,0)+',toOOB'
                    toDead   = key(NONE,0)+',toDead'
                    markElim = key(NONE,0)+',eliminated'
                    traits.extend([
                        DynamicPropertyTrait(
                            ['',markElim,DynamicPropertyTrait.DIRECT,'{true}'],
                            name = 'Eliminated',
                            numeric = True,
                            value = False),
                        SendtoTrait(
                            name        = '',
                            key         = toOOB,
                            mapName     = oob['mapName'],
                            boardName   = oob['mapName'],
                            destination = 'R',
                            region      = parent['location'],
                            restoreName = '',
                            restoreKey  = '',
                            position    = name),
                        SendtoTrait(
                            name        = '',
                            key         = toDead,
                            mapName     = 'DeadMap',
                            boardName   = f'{Faction} pool',
                            destination = SendtoTrait.LOCATION,
                            restoreName = '',
                            restoreKey  = ''),
                        TriggerTrait(
                            name        = 'Eliminate',
                            command     = 'Eliminate',
                            key         = eliminateKey,
                            actionKeys  = [markElim,eliminateCmd]),
                        TriggerTrait(
                            name        = 'Eliminate to OOB',
                            command     = '',
                            key         = eliminateCmd,
                            actionKeys  = [toOOB],
                            property    = f'{{{optNight}}}'),
                        TriggerTrait(
                            name        = 'Eliminate to pool',
                            command     = '',
                            key         = eliminateCmd,
                            actionKeys  = [toDead],
                            property    = f'{{!{optNight}}}'),
                        TriggerTrait(
                            name       = 'Alias for Ctrl-E',
                            command    = '',
                            key        = stepKey,
                            actionKeys = [eliminateKey])])

            # Set traits 
            traits.append(basic)
            piece.setTraits(*traits)

            

    # --- Remove piece window - not needed ---------------------------
    for pw in pwindows.values():
        print(f'Piece window: {pw["name"]}')
        tab = pw.getTabs().get("Counters",None)
        if not tab:
            continue

        panels = tab.getPanels()
        union  = panels.get("Union",None)
        conf   = panels.get("Confederate",None)
        if union:
            tab.remove(union)
        if conf:
            tab.remove(conf)
        # print(f'Lists: {pw.getLists()}')
        # print(f'Tabs: {pw.getTabs()}')
        # print(f'Combos: {pw.getCombos()}')
        # print(f'Panels: {pw.getPanels()}')
        # game.remove(pw)
    
#
# EOF
#
