#!/usr/bin/env python
#
# Note, image dimensions _must_ be multiples of 2 or AV(FFMPEG) will fail
#
# Needs imageio, asyncio, pillow, numpy
# Needs pdfjam, pdfinfo
# 
# ====================================================================
def parse_date(value):
    from datetime import datetime

    try:
        return datetime.strptime(value,'%a %b %d %H:%M:%S %Y %Z')
    except Exception as e:
        raise RuntimeError(f'Failed to parse date "{value}"') from e

# --------------------------------------------------------------------
def parse_size(value):
    parts   = value.split()
    number  = int(parts[0])
    factors = {'bytes': 1,
               'kilobytes': 1024,
               'metabytes': 1024**2,
               'gigabytes': 1024**3,
               'terabytes': 1024**4,
               'petabytes': 1024**5 }
    factor  = factors.get(parts[1].strip(),1)
    return number * factor

# --------------------------------------------------------------------
def parse_dimensions(value,ppi=150,verbose=False):
    from re import match

    m = match(r'([0-9.]+) x ([0-9.]+) (\S+)',value)
    if not m:
        raise RuntimeError(f'Failed to parse file dimensions: "{value}"')

    pt_per_px   = 0.74999943307122
    pt_per_inch = 71.995
    cm_per_inch = 2.54
    try:
        factors = {'pts':    1,
                   'inches': pt_per_inch,
                   'cms':    pt_per_inch / cm_per_inch,
                   'px':     pt_per_px}
        factor  =  factors.get(m[3],1)
        width   =  float(m[1]) 
        height  =  float(m[2])

        #if verbose:
        #     print(f'WxH={width} x {height} pt, factor={factor}')
        
        width   *= factor * ppi / pt_per_inch
        height  *= factor * ppi / pt_per_inch
        
        # if verbose:
        #     print(f'WxH={width} x {height} pt, factor={ppi/pt_per_inch}')
        
        return int(round(width)),int(round(height))
    except Exception as e:
        raise RuntimeError(f'Failed to parse file dimensions: "{value}"') \
            from e
    
# --------------------------------------------------------------------
def pdf_info(filename,
             resolution = 150,
             owner_pwd  = None,
             user_pwd   = None,
             timeout    = None,
             verbose    = False):
    from subprocess import Popen, PIPE, TimeoutExpired
    
    cmd = ['pdfinfo']
    if owner_pwd: cmd.extend(['-opw', owner_pwd])
    if user_pwd:  cmd.extend(['-upw', user_pwd])
    cmd.append(filename)

    if verbose:
        print(' '.join(cmd))
        
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE)

    try:
        out, err = proc.communicate(timeout=timeout)
    except TimeoutExpired as e:
        proc.kill()
        outs, errs = proc.communicate()
        raise RuntimeError("pdfinfo timeout") from e

    db = {}
    for line in out.decode().split('\n'):
        fields = line.split(':')
        key    = fields[0]
        value  = ':'.join(fields[1:]).strip()

        if not key or not value:
            continue
        elif key.endswith('Date'):
            value = parse_date(value)
        elif key == 'File size':
            value = parse_size(value)
        elif key == 'Page size':
            value = parse_dimensions(value,resolution,verbose)
        elif value in ['yes','no']:
            value = value == 'yes'
        elif value == 'none':
            value = None
        else:
            try:
                value = int(value)
            except:
                try:
                    value = float(value)
                except:
                    pass

        db[key] = value

    return db

# --------------------------------------------------------------------
def pdf_dests(filename,
              owner_pwd  = None,
              user_pwd   = None,
              timeout    = None,
              verbose    = False):
    from subprocess import Popen, PIPE, TimeoutExpired
    
    cmd = ['pdfinfo','-dests']
    if owner_pwd: cmd.extend(['-opw', owner_pwd])
    if user_pwd:  cmd.extend(['-upw', user_pwd])
    cmd.append(filename)

    if verbose:
        print(' '.join(cmd))
        
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE)

    try:
        out, err = proc.communicate(timeout=timeout)
    except TimeoutExpired as e:
        proc.kill()
        outs, errs = proc.communicate()
        raise RuntimeError("pdfinfo timeout") from e

    db = {}

    from re import match
    
    for line in out.decode().split('\n')[1:]:
        m = match(r' *([0-9]+) \[.*\] "(.*)"', line)
        if not m:
            continue

        page  = int(m[1])
        label = m[2]

        m = match(r'([0-9]+)@([a-zA-Z]+)@(.*)\.2',label)
        if not m:
            continue

        db[page] = { 'turn': int(m[1]),
                     'faction': m[2],
                     'phase':   m[3].replace('_',' ') }

    return db
        
                  
    
# ====================================================================
def pdf_page(filename,
             page,
             resolution = 150,
             x0         = 0,
             y0         = 0,
             width      = 0,
             height     = 0,
             owner_pwd  = None,
             user_pwd   = None,
             timeout    = None,
             verbose    = False):
    from subprocess import Popen, PIPE, TimeoutExpired
    
    cmd = ['pdftocairo',
           '-png','-r',str(resolution),'-singlefile',
           '-f',str(page),'-l',str(page),
           '-x',str(x0),'-y',str(y0),'-W',str(width),'-H',str(height)]

    if owner_pwd: cmd.extend(['-opw', owner_pwd])
    if user_pwd:  cmd.extend(['-upw', user_pwd])

    cmd.extend([filename,'-'])
    
    if verbose: 
        print(f'{page:4d}: {" ".join(cmd)}')
        
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE)

    try:
        out, err = proc.communicate(timeout=timeout)
    except TimeoutExpired as e:
        proc.kill()
        outs, errs = proc.communicate()
        raise RuntimeError("pdftocairo timeout") from e

    return out

# --------------------------------------------------------------------
def bytes_to_image(bytes):
    from PIL import Image
    from io import BytesIO
    
    return Image.open(BytesIO(bytes)).convert('RGBA')

# --------------------------------------------------------------------
async def pdf_image(filename,
                    page,
                    resolution = 150,
                    x0         = 0,
                    y0         = 0,
                    width      = 0,
                    height     = 0,
                    save       = False,
                    owner_pwd  = None,
                    user_pwd   = None,
                    timeout    = None,
                    verbose    = False):
    bytes = pdf_page(filename,
                     page,
                     resolution = resolution,
                     x0         = x0,
                     y0         = y0,
                     width      = width,
                     height     = height,
                     owner_pwd  = owner_pwd,
                     user_pwd   = user_pwd,
                     timeout    = timeout,
                     verbose    = verbose)
    img = bytes_to_image(bytes)
    if verbose:
        print(f'{page:4d}: {img.size}')
        
    if save:
        img.save(f'{page:04d}.png')

    return page, img
    
# --------------------------------------------------------------------
async def pdf_images(filename,
                     first_page = 1,
                     last_page  = 0,
                     save       = False,
                     resolution = 150,
                     crop       = None,
                     bounding   = None,
                     scale      = None,
                     owner_pwd  = None,
                     user_pwd   = None,
                     timeout    = None,
                     verbose    = False):
    from asyncio import TaskGroup
    from numpy import array
    
    info = pdf_info(filename,
                    resolution = resolution,
                    owner_pwd  = owner_pwd,
                    user_pwd   = user_pwd,
                    timeout    = timeout,
                    verbose    = verbose)
    dest = pdf_dests(filename, 
                     owner_pwd  = owner_pwd,
                     user_pwd   = user_pwd,
                     timeout    = timeout,
                     verbose    = verbose)

    if not 'Pages' in info:
        raise RuntimeError(f'Number of pages in "{filename}" unknown!')

    if verbose:
        from pprint import pprint
        pprint(info)
        pprint(dest)

    w0, h0   = info['Page size']
    bb       = parse_crop_bb(crop,bounding)
    if bb['width'] != 0 and bb['height'] != 0:
        w0     = bb['width']
        h0     = bb['height']
    sx, sy     = fix_scale([w0,h0], scale)
    frames     = []
    first_page = max(first_page,1)
    last_page  = info['Pages'] if last_page <= 0 else \
        min(last_page,info['Pages'])

    tasks = []
    async with TaskGroup() as tg:        
        for page in range(first_page,last_page+1):
            tasks.append(tg.create_task(pdf_image(filename,
                                                  page,
                                                  resolution = resolution,
                                                  **bb,
                                                  save       = save,
                                                  owner_pwd  = owner_pwd,
                                                  user_pwd   = user_pwd,
                                                  timeout    = timeout,
                                                  verbose    = verbose)))

    frames   = [None]*len(tasks)
    chapters = [None]*len(tasks)
    for task in tasks:
        page, img       = task.result()
        frameno         = page-first_page
        frames[frameno] = array(img)
        meta            = dest.get(page,None)
        if meta is None: continue

        turn              = meta['turn']
        faction           = meta['faction']
        phase             = meta['phase']
        chapters[frameno] = f'Turn {turn}, {faction}, {phase}'

    return frames, chapters, info

# --------------------------------------------------------------------
def fix_scale(dimensions, scale):
    w0, h0 = dimensions
    sx, sy = 1, 1

    if not scale:
        pass
    elif isinstance(scale,list) or isinstance(scale,tuple):
        if len(scale) == 1:
            sx = sy = scale
        elif len(scale) == 2:
            sx, sy = scale
        else:
            raise ValueError(f'Scale must be a single number or pair')
    else:
        sx = sy = float(scale)

    def find_scale(s,l,epsilon=.0001,factor=.99):
        t = s
        while t > epsilon:
            if int(t * l) % 2 == 0:
                return t

            t *= factor
        raise ValueError(f'Could not find scale factor, starting at {s}, '
                         f'to make {l} even (factor={factor})')

    sx = find_scale(sx, w0)
    sy = find_scale(sy, h0)

    return sx, sy

# --------------------------------------------------------------------
def parse_crop_bb(crop,bounding):
    x0 = 0
    y0 = 0
    w  = 0
    h  = 0
    
    if not crop:
        if not bounding:
            pass
        else:
            x0, y0 = bounding[:2]
            w      = bounding[3]-x0
            h      = bounding[4]-y0
    elif isinstance(crop,list) or isinstance(crop,tuple):
        if len(crop) == 1:
            w = h = crop[0]
        elif len(crop) == 2:
            x0, y0 = crop
        elif len(crop) == 4:
            x0, y0, w, h = crop
        else:
            raise ValueError(f'Crop box must be 1, 2, or 4 numbers')
    else:
        w = h = crop

    return {'x0': x0, 'y0': y0, 'width': w, 'height': h}

# ====================================================================
def make_video(frames,chapters,info,filename,fps=.5,verbose=False):
    from imageio.v3 import imopen
    from subprocess import Popen, PIPE, TimeoutExpired
    from pathlib import Path
    
    # Read meta data as
    #
    #   ffmpeg -i <video> -f ffmetadata <output>
    #
    # Set meta data with
    #
    #   ffmpeg -i <video> -i <metadata> -map_metadata 1 -codec copy <output>
    #
    # (What about `-map_chapters <input no>'?)
    with open('chapters.txt', 'w') as ch:
        ch.write(f''';FFMETADATA1
title={info.get("Title","")}
artist={info.get("Author","")}        
copyright=(c) {info.get("Author","")}, CC4-attribution license
        
''')
        for frameNo,chapter in enumerate(chapters):
            if chapter is None:
                if chapter == 0:
                    chapter = 'Start'
                elif chapter == len(chapters)-1:
                    chapter = 'End'
                else:
                    chapter = f'Frame {frameNo}'

            ch.write(f'''[CHAPTER]
TIMEBASE=1/1000
START={int(frameNo/fps*1000)}
END={int((frameNo+1)/fps*1000)-1}
title={chapter}
            
''')

    
    codecs = {'.webp' : 'libwebp_anim',
              '.mp4':  'libx264'}
    codec  = codecs.get(Path(filename).suffix,codecs['.mp4'])
    tmp    = ('tmp_' if codec != 'libwebp_anim' else '')+filename

    if verbose:
        print(f'Create video file "{tmp}"')
    with imopen(tmp,'w',plugin='pyav') as out:
        out.init_video_stream(codec,fps=fps)
 
        for no,frame in enumerate(frames):
            if verbose:
                print(f'Frame # {no:4d}')
            out.write_frame(frame,pixel_format='rgba')

    if tmp == filename:
        if verbose:
            print(f'Output video: {filename}')
        return

    cmd = ['ffmpeg',
           '-y',
           '-v',
           '40' if verbose else '8',
           '-i',
           'tmp_'+filename,
           '-i',
           'chapters.txt',
           '-map_metadata',
           '1',
           '-codec',
           'copy',
           filename]
    if verbose:
        print(' '.join(cmd))
        
    proc = Popen(cmd)

    try:
        out, err = proc.communicate(timeout=None)
    except TimeoutExpired as e:
        proc.kill()
        outs, errs = proc.communicate()
        raise RuntimeError("ffmpeg timeout") from e
            

    if verbose:
        print(f'Output video: {filename}')


# ====================================================================
async def doit(**kwargs):

    frames,chapters,info = \
        await pdf_images(kwargs['input'].name,
                         first_page  = kwargs['first_page'],
                         last_page   = kwargs['last_page'],
                         save        = kwargs['save_pngs'],
                         resolution  = kwargs['resolution'],
                         crop        = kwargs['crop'],
                         bounding    = kwargs['bounding_box'],
                         owner_pwd   = kwargs['owner_password'],
                         user_pwd    = kwargs['user_password'],
                         timeout     = kwargs['timeout'],
                         verbose     = kwargs['verbose'])


    if not kwargs['no_video']:
        make_video(frames,
                   chapters,
                   info,
                   kwargs['output'],
                   fps     = kwargs['frames_per_second'],
                   verbose = kwargs['verbose'])
    
# ====================================================================
#
# ./makeMovie.py -v -r 100 -c 104 32 1440 1094
#
if __name__ == '__main__':
    from argparse import ArgumentParser, FileType,\
        RawDescriptionHelpFormatter
    from asyncio import run
    
    ap = ArgumentParser(description='Make MP4 from PDF',
                        formatter_class=RawDescriptionHelpFormatter,
                        epilog='''
Crop box is one of

    S       - Set WIDTHxHEIGHT=SxS and OFFSET=(0,0)
    W H     - Set WIDTHxHEIGHT=WxH and OFFSET=(0,0)
    W H X Y - Set WIDTHxHEIGHT=WxH and OFFSET=(X,Y)

Bounding box is

    X0 Y0 X1 Y1 - Set WIDTHxHEIGHT=(X1-X0)x(Y1-Y0) and OFFSET=(X0,Y0)

Remember that (X0,Y0) is the upper left corner
''')
    ap.add_argument('input',nargs='?',default='movesA3.pdf',
                    type=FileType('r'),help='Input PDF')
    ap.add_argument('output',nargs='?',default='movesA3.mp4',
                    type=str,help='Output MP4')
    ap.add_argument('-f','--first-page',type=int,default=1,metavar='PAGE',
                    help='First page to convert')
    ap.add_argument('-l','--last-page',type=int,default=-1,metavar='PAGE',
                    help='Last page (inclusive) to convert (<=0: all)')
    ap.add_argument('-v','--verbose',action='store_true',
                    help='Be verbose')
    ap.add_argument('-r','--resolution',type=int,default=100,
                    metavar='PPI',
                    help='Resolution, pixel per inch, to render PDF at')
    ap.add_argument('-F','--frames-per-second',type=float,default=.5,
                    metavar='FPS',
                    help='Frames per second')
    ap.add_argument('-s','--save-pngs',action='store_true',
                    help='Save individual frames as PNG')
    ap.add_argument('--owner-password',type=str,metavar='PASSWD',
                    help='Owner\'s password for PDF')
    ap.add_argument('--user-password',type=str,metavar='PASSWD',
                    help='User\'s password for PDF')
    ap.add_argument('--timeout',type=int,
                    help='Time-out in seconds of sub-processes')
    ap.add_argument('--no-video',action='store_true',
                    help='Do not create video')
    ap.add_argument('-c','--crop',type=int,nargs='+',
                    default=[104, 32, 1440, 1094],
                    help='Specify crop box for frames')
    ap.add_argument('-b','--bounding-box',type=int,nargs=4,
                    metavar=('X0','Y0','X1','Y1'),
                    help='Specify bounding box for frames')
    

    args = ap.parse_args()
    args.input.close()
    
    run(doit(**vars(args)))
               
#
# EOF
#

                        
