#!/usr/bin/env python
#
# needs pandas
#
# ====================================================================
def append2key(d,k,v):
    if k not in d:
        d[k] = []
    d[k].append(v)

# --------------------------------------------------------------------
def append2list(l,v):
    if l is None:
        l = []
    l.append(v)
    return l

# --------------------------------------------------------------------
def cellValue(x):
    from math import isnan 
    if isinstance(x,float):
        if isnan(x):
            return None,None
        return None,int(x)
    
    if isinstance(x,int):
        return None,x
    
    return x,None

# ====================================================================
def readMoves(filename='ref/TheGeneralVol25No5p6-moves.ods'):
    from pandas import read_excel

    df      = read_excel(filename,0)
    dc      = df.transpose().to_dict('list')
    perUnit = { v[0]: [cellValue(c)[0] for c in v[1:]] for v in dc.values()
                if cellValue(v[0])[0] }

    dc      = df.to_dict('list')
    perTurn = list(dc.values())

    turns   = {}
    units   = perTurn[0]
    order   = ['hqa', 'hqd', 'hqc', 'id', 'cb', 'arb', 'art']

    def key(u):
        v = order.index(u[0].split()[-1])
        return f'{v} {u[0]}'

    def sort_moves(moves):
        for mhex,munits in moves.items():
            moves[mhex] = sorted(munits,key=key,reverse=True)
    
    for turnNo0, turn in enumerate(perTurn[1:]):
        usmoves              = {}
        csmoves              = {}

        for unit,cell in zip(units,turn):
            hex,_ = cellValue(cell)
            if hex is None:
                continue

            append2key(csmoves if unit.startswith('cs') else usmoves,hex,
                       [unit,False])


        sort_moves(usmoves)
        sort_moves(csmoves)
        turns[turnNo0+1]     = {'cs': {'moves': csmoves},
                                'us': {'moves': usmoves} }
    return perUnit, turns


# ====================================================================
def readCombats(turns,filename):
    from pandas import read_excel
    from pprint import pprint
    
    df = read_excel(filename,1)

    turn       = None
    combat     = None
    attackers  = None
    defenders  = None
    
    def storeCombat(turn,combat):
        if combat:
            if turn:
                # print(combat)
                if combat['attackers'][0][0].startswith('cs'):
                    append2key(turn['cs'],'combats',combat)
                else:
                    append2key(turn['us'],'combats',combat)
            else:
                raise RuntimeError('No turn to store combat in')
        combat = None

    def fixAfter(after):
        if not after: return after

        if after.startswith('step,'):
            after =  after.replace('step,','').strip()
            after += ', step'
        after = after.replace(',step',', step')
        after = after.replace('Elim','elim')
        after = after.replace('elilm','elim')
        return after 
                
    for no, series in df.iterrows():
        row = list(series.to_dict().values())

        # Check
        turnCol,_ = cellValue(row[0])
        
        if turnCol is not None:
            storeCombat(turn,combat)
            combat = None
            
            turnNo = int(turnCol.replace('Turn ',''))
            turn   = turns[turnNo]
            turn['cs']['combats'] = []
            turn['us']['combats'] = []
            

        iniCol,_ = cellValue(row[5])
        if iniCol is not None:
            # print('=== Initiative change ===')
            initialitve = iniCol

        attCol,attNum1 = cellValue(row[1])
        attAft,_       = cellValue(row[2])
        defCol,defNum1 = cellValue(row[3])
        defAft,defNum2 = cellValue(row[4]) 

        # Attacker and defender numbers, rolls or result
        if attNum1 and defNum1:
            # New combat
            # print(f'Rolls or result {combat}')
            if combat is None:
                combat = {'attackers': attackers,
                          'defenders': defenders,
                          'tries': [] }
            # Reset attackers 
            attackers = None
            defenders = None
                
            # after for defender is string -> result
            if defAft:
                result  = defAft
                attRoll = attNum1
                defRoll = defNum1

                # print(f'Got result: {result}')
                combat['tries'][-1].update({
                    'attackerRoll': attRoll,
                    'defenderRoll': defRoll,
                    'result':       result})
                    
            else:
                attStr  = attNum1
                defStr  = defNum1
                diff    = defNum2

                # print(f'Got differential: {diff}')
                combat['tries'].append({
                    'attackerStrength': attStr,
                    'defenderStrength': defStr,
                    'differential':     diff })

        else:
            storeCombat(turn,combat)

            # print('Attacker {attCol} Defender {defCol}; {combat}')
            combat = None
            if attCol is not None:
                attAft    = fixAfter(attAft)
                attackers = append2list(attackers, [attCol,attAft])
            if defCol is not None:
                defAft    = fixAfter(defAft)
                defenders = append2list(defenders, [defCol,defAft])
            
# ====================================================================
def rev_moves(moves):
    return {unit : [hex,flip] for hex,units in moves.items()
            for unit,flip in units}

# --------------------------------------------------------------------
def res_moves(moves):
    # print(list(moves.values()))
    ret = {hex: [] for hex,_ in moves.values()}
    for unit, (hex,flip) in moves.items():
        if hex is not None:
            ret[hex].append([unit,flip])
    return {hex:units for hex,units in ret.items() if units}

# --------------------------------------------------------------------
def do_after(csmoves,usmoves,side,eliminated,steps):
    for unit,after in side:
        # if unit == 'us randolph 3 arb':
        #     print(unit,after)
        if after is None:
            continue
        
        moves = csmoves if unit.startswith('cs') else usmoves
        step  = 'step' in after or moves[unit][1]
        after = after.replace(', step','')
        elim  = 'elim' in after.lower()
        if elim: after = None

        if elim:
            # print(f'Unit {unit} eliminated')
            moves[unit] = [None,None]
            eliminated.append(unit)
        elif after:
            # full/half state only changes when hex after changes
            # print(f'Unit {unit} to {after}')
            moves[unit] = [after,step]
        if step:
            steps.append(unit)
        
# --------------------------------------------------------------------
def do_post(turns):
    from copy import deepcopy
    
    prevus     = rev_moves(turns[1]['us']['moves'])
    eliminated = []
    steps      = []
    def fixFlip(moves,target):
        for unit,(hex,flip) in moves.items():
            if not flip: continue
            for hx,units in target.items():
                if [unit,not flip] in units:
                    #print(f'{unit} flipped')
                    #print(prevus[hx])
                    target[hx] = [[u,flip if u == unit else f]
                                  for u,f in target[hx]]
                    #print(prevus[hx])
    
    for turnNo, turn in turns.items():
        csmoves = rev_moves(turn['cs']['moves'])
        usmoves = prevus
        
        for faction, phases in turn.items():
            combats = phases.get('combats',[])

            if faction == 'us':
                prevus  = phases['moves']
                fixFlip(usmoves,prevus)
                prevus  = rev_moves(prevus)
                usmoves = prevus
                
            for combat in combats:
                attackers = combat['attackers']
                defenders = combat['defenders']

                do_after(csmoves,usmoves,attackers,eliminated,steps)
                do_after(csmoves,usmoves,defenders,eliminated,steps)

            if faction == 'us':
                tmp = deepcopy(prevus)
                for unit,(hex,flip) in usmoves.items():
                    if hex is not None:
                        tmp[unit] = (hex,flip)
                        continue
                    
                    # print(f'Unit {unit} is eliminated ({hex},{flip})')
                    del tmp[unit]
                #print(tmp is prevus)
                prevus = tmp
                            
            # if turnNo == 9 and faction == 'us':
            #     print(usmoves)
            # if turnNo == 10 and faction == 'cs':
            #     print(usmoves)


            end = {}
            end.update(res_moves(csmoves))
            #if not (turnNo == 1 and faction == 'cs'):
            if turnNo > 1 or faction == 'us':
                end.update(res_moves(usmoves))

            # if turnNo == 9 and faction == 'us':
            #     print(usmoves.get('us randolph 3 abd','None'))
            #     print(res_moves(usmoves).get('I4','Nothing'))
            #     print(end.get('I4','Nothing'))
            # if turnNo == 10 and faction == 'cs':
            #     print(usmoves.get('us randolph 3 abd','None'))
            #     print(res_moves(usmoves).get('I4','Nothing'))
            #     print(end.get('I4','Nothing'))
                
            
            phases['end'] = end
            phases['eliminated'] = eliminated.copy()

            if turnNo+1 in turns:
                nextcs = turns[turnNo+1]['cs']['moves']
                fixFlip(csmoves,nextcs)
                nextus = turns[turnNo+1]['us']['moves']
                fixFlip(usmoves,nextus)
                
                
# --------------------------------------------------------------------
def fillMoves(turns):
    prev = {'us':  {},
            'cs':  {} }
    other = {'cs': 'us',
             'us': 'cs' }

    for turnNo,turn in turns.items():
        for faction, phases in turn.items():
            opponent      = other[faction]
            moves         = phases['moves']
            omoves        = prev[opponent]
            prev[faction] = phases['end']

            for hex,units in omoves.items():
                if not units[0][0].startswith(opponent):
                    continue
                if hex in moves:
                    raise ValueError(f'Hex {hex} already there')
                moves[hex] = units
            
# --------------------------------------------------------------------
def fillWhere(turns):
    from pprint import pprint 
    for turnNo,turn in turns.items():
        #print(turnNo)
        for faction, phases in turn.items():
            moves   = rev_moves(phases['moves'])
            combats = phases.get('combats',[])
            #print('',faction)
            
            for combat in combats:
                attackers = combat['attackers']
                defenders = combat['defenders']

                try:
                    fromHex   = {moves[unit[0]][0] for unit in attackers}
                    toHex     = {moves[unit[0]][0] for unit in defenders}
                    #print(fromHex,'->',toHex)
                except Exception as e:
                    print(turnNo,faction)
                    pprint(moves)
                    pprint(phases['moves'])
                    pprint(attackers)
                    pprint(defenders)
                    print(e)
                    raise
                
                combat['from'] = list(fromHex)
                combat['to']   = list(toHex)
                
    
# --------------------------------------------------------------------
def doit(input,output):
    from json import dump
    
    unit_history, turns = readMoves(input)
    readCombats(turns,input)
    do_post(turns)
    fillMoves(turns)
    fillWhere(turns)
    
    dump(turns,output,indent=1)
            
# --------------------------------------------------------------------
if __name__ == '__main__':
    from argparse import ArgumentParser, FileType

    ap = ArgumentParser(description='Read spread sheet')
    ap.add_argument('input',help='Input spreadsheet',nargs='?',
                    default='ref/TheGeneralVol25No5p6-moves.ods',
                    type=FileType('r'))
    ap.add_argument('output',help='Output JSON',nargs='?',
                    default='moves.json',type=FileType('w'))

    args = ap.parse_args()
    inname = args.input.name
    args.input.close()
    
    doit(inname,args.output)
