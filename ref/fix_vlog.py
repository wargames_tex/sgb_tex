#!/usr/bin/env python

import sys
sys.path.append("../wargame_tex/utils")

def _read_it(parent):
    from wgexport import SaveIO
    return SaveIO.readSave(parent,alsometa=True)

def _write_save(lines,savedata,moduledata,filename='tmp.vlog'):
    from wgexport import SaveIO
    SaveIO.writeSave(filename,
                     key         = 0xAA,
                     lines       = lines,
                     savedata    = savedata,
                     moduledata  = moduledata)
        
def _read_save(file,sub):
    from zipfile import ZipFile, ZIP_DEFLATED
    from sys import stderr 

    fname = file.name
    file.close()

    key, lines = None, None
    try:
        key, lines, sdata, mdata = _read_it(fname)
    except Exception as e:
        print(e)
        pass

    if key is None and lines is None:
        if sub is None or sub == '':
            raise RuntimeError(f'{fname} is probably a module '
                               f'but not internal filename given')
        try:
            print(f'Try to read {fname}/{sub} as a save container')
            with ZipFile(fname,'r') as vmod:
                key, lines, sdata, mdata = _read_it(vmod.open(sub,'r'))
            
        except Exception as e:
            print(e,file=stderr)
            raise e

    return key,lines,sdata,mdata

def vassal_dump(file,
                sub     = None,
                output  = 'tmp.vlog',
                last    = -1,
                verbose = False,
                dx      = 0,
                raw     = None):

    key, lines, sdata, mdata = _read_save(file,sub)

    if last is not None and last == 0:
        last = None


    if raw is not None:
        lines = raw.readlines()
        lines = [l.strip('\n') for l in lines]
        
    if verbose:
        from math import log10
        nlines = len(lines)
        f      = int(log10(nlines)+1)
        #print(f'Got {nlines} from save')
        for i,l in enumerate(lines[:last]):
            # print(f'{i:{f}d} {l}')
            print(f'{l}')

    if output is not None and output != '':
        print(f'Writing lines 0 to {last} to {output}')
        _write_save(lines[:last],sdata,mdata,filename=output)
        
    
if __name__ == '__main__':
    from argparse import ArgumentParser, FileType

    ap = ArgumentParser(description='Show content of a VASSAL save or log file')
    ap.add_argument('input',
                    type=FileType('r'),
                    help='Save or log file to show')
    ap.add_argument('--output','-o',type=str,default='foo.vlog',
                    help='vlog to write to')
    ap.add_argument('--sub-file','-s',type=str,default='',
                    help='Sub vlog/vsav file if input is a module')
    ap.add_argument('--last','-n',type=int,default=0,
                    help='How may lines to show')
    ap.add_argument('--verbose','-v',action='store_true',
                    help='Whether to show original content')
    ap.add_argument('--offset-x','-x',type=int,default=0,
                    help='X offset')
    ap.add_argument('--raw','-r',type=FileType('r'),default=None,
                    help='Raw lines')

    args = ap.parse_args()

    vassal_dump(file    = args.input,
                sub     = args.sub_file,
                output  = args.output,
                last    = args.last,
                verbose = args.verbose,
                dx      = args.offset_x,
                raw     = args.raw)
    
