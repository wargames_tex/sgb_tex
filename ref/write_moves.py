#!/usr/bin/env python
#
# 62 pages of boards - need two more
#
# ====================================================================
def open_out(outp):
    from pathlib import Path
    tabloid = 'Tabloid' in outp.name
    jobname = Path(outp.name).stem
    
    # outp = open(filename,'w')

    outp.write(fr'''\documentclass{{article}}
\usepackage{{sgbexa}}    
\usepackage[a3paper,landscape,margin=6mm]{{geometry}}
\if\jobis{{{jobname}Tabloid}}
  \tabloidsetup
\fi
\hypersetup{{
    pdfauthor={{Christian Holm Christense}},
    pdftitle={{Replay from The General Vol.25, \#5, p6}},
    pdfsubject={{Gettysburg - Smithsonian Edition}},
    pdfkeywords={{Gettysburg,Smithsonian,Avalon Hill,Game,Wargame,Replay}}}}
%%
%%
%%
\newif\ifstandalone
\input{{tables}}
%%
%%
%%    
\begin{{document}}    
\phasefront{{
   Replay from\\
     \textit{{The General}}~\textbf{{Vol.25}}, \# 5, p6}}
''')
    clearpage(outp)
    return outp


# --------------------------------------------------------------------
def close_out(outp):#36.7cm}{27.9cm
    gen = r'https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2025%20No%205.pdf\#page=6'
    sgb = r'https://wargames_tex.gitlab.io/wargame_www/games.html\#gettysburg-smithsonian'
    outp.write(fr'''\phaseback{{
  Full text, including commentary of the replay can be found in\\[1ex]
  $\bullet$\space
  Martin,R.A., Cluck,B.A., and Taylor,S.C., Martin, B.,
  ``Gettysburg '88 (Series Replay)'',\\
  \phantom{{$\bullet$}}\space
  \textit{{The General}}, \textbf{{Vol.25}}, \#5, p6-14\\
  \phantom{{$\bullet$}}\space
  \url{{{gen.replace("%",r"\%")}}}\\[1ex]
  Print'n'Play version of the game used available from\\[1ex]
  $\bullet$\space
  \url{{{sgb}}}}}
%%
%%
%%
\end{{document}}
%% Local Variables:
%% TeX-engine: luatex    
%% End:
''')

# ====================================================================
def coord(hex):
    try:
        colnam = hex[0]
        rownam = hex[1:]
        colno  = ord(colnam)-ord('A')+1
        rowno  = int(rownam)
    except Exception as e:
        print(f'Failed to parse {hex}: {e}')
        raise
        
    return f'(hex cs:c={colno},r={rowno})'

# ====================================================================
def start_turn(outp,turn,faction='cs',phase=''):
    turnrow = (turn - 1) // 8
    turncol = ((turn - 1) % 8)
    xoff    = -.3 + 1.4 * turncol
    yoff    = -1.75 - 1.5 * turnrow
    chit    = 'game turn'+('' if faction == 'cs' else ' flipped')
    loc     = f'${coord("O11")}+({xoff},{yoff})$'
    facname = 'Confederate' if faction == 'cs' else 'Union'
    outp.write('%\n%\n%\n')
    outp.write(fr'\message{{Turn {turn}, {facname}, {phase}}}'+'\n')
    outp.write(fr'\pdfbookmark[2]{{{phase}}}{{{turn}@{facname}@{phase.replace(" ","_")}}}'+'\n')
    outp.write(r'\begin{phase}'+'\n')
    outp.write(fr'  \node[board header] {{Turn {turn}, {facname}, {phase}}};'+'\n')
    outp.write(fr'  \coordinate (turn) at ({loc});'+'\n')
    outp.write(fr'  \chit[{chit}](turn);'+'\n')

# --------------------------------------------------------------------
def clearpage(outp):
    outp.write(r'''\clearpage
%%
%%
%%
''')    

# --------------------------------------------------------------------
def end_turn(outp):
    outp.write(r'\end{phase}'+'\n')

# ====================================================================
def state(outp,places,turn,faction,title,init):
    start_turn(outp, turn, faction, title)
    for hex,units in sorted(places.items(),reverse=True):
        where = coord(hex)
        chits = '%\n      '+\
            ',%\n      '.join([fr'\noexpand\chit[{u[0]+(" flipped"
                             if u[1] else "")}]'
                             for u in units])
        outp.write(fr'    \stackit{where}{{{chits}}}'+'\n')

    init_side = 'initiative'+ ('' if init[0] == 'cs' else ' flipped')
    outp.write(fr'    \chit[{init_side}](hex cs:c=12,r=12);'+'\n')

# ====================================================================
def unit_list(units):
    return '%\n         '.join([r'\tabchit'+
                                fr'[{u[0]+(" flipped" if u[1] else "")}]'
                           for u in units])

# --------------------------------------------------------------------
def row_col(outp,no):
    outp.write(f'    \\{"def" if no % 2 == 0 else "alt"}row\n')

# --------------------------------------------------------------------
def elim_table(outp,eliminated,turn,faction):
    if not eliminated:
        return

    elim = sorted(eliminated)
    elimcs = [unit for unit in elim if unit.startswith('cs')]
    elimus = [unit for unit in elim if unit.startswith('us')]
    elim1  = '%\n      '.join([fr'\tabchit[{unit}]' for unit in elimcs])
    elim2  = '%\n      '.join([fr'\tabchit[{unit}]' for unit in elimus])
    if elim1: elim1 += r'\\'+'\n    '
    if elim2: elim2 += r'\\'
    if elim1 and elim2: elim2 =  r'\altrow'+elim2
    outp.write(fr'''\node[elim table]{{
    \begin{{eliminated}}
      \defrow{elim1}{elim2}
    \end{{eliminated}}}};
''')
    
# --------------------------------------------------------------------
def state_table(outp,places,turn,faction,title):
    outp.write(r'''\node[state table] (states) {
  \begin{states}'''+'\n')
    
    def key(hexunit):
        hex,units = hexunit
        return units[0][0][:2]+hex
    
    pl = sorted(places.items(), key=key)
    for no, (hex,units) in enumerate(pl):
        chits = unit_list(units[::-1])
        e = r'\\' #if no % 4 == 3 else '&'
        row_col(outp,no)
        outp.write(fr'    {hex} & {chits}'+e+'\n')
    outp.write(r'    \hline'+'\n'+r'  \end{states}};'+'\n')
    
        
# --------------------------------------------------------------------
def do_moves(outp,moves,turn,faction,eliminated,init):
    if not moves: return

    state(outp, moves, turn, faction, 'Movement',init)
    end_turn(outp)
    clearpage(outp)

# --------------------------------------------------------------------
def combat_list(units,states):

    l = [[unit,states[unit]]  for unit,_ in units]
    return unit_list(l)
            
# --------------------------------------------------------------------
def shortstack(*args):
    return r'\begin{tabular}{@{}c@{}}'+\
        r'\\'.join([str(a) for a in args])+\
        r'\end{tabular}'

# --------------------------------------------------------------------
def combat_table(outp,combats,places,turn,faction,init):
    from pprint import pprint
    if not combats: return

    states = {unit : flip for _,units in places.items()
              for unit,flip in units}

    ncombat = len(combats)
    env     = 'combat'
    if ncombat > 1:
        env += 's'

    outp.write(fr'''\node[combat table] (combats) {{
  \begin{{{env}}}'''+'\n')
    
    for no,combat in enumerate(combats):
        fr        = ','.join(combat.get('from',[]))
        to        = ','.join(combat.get('to',[]))
        attackers = combat['attackers']
        defenders = combat['defenders']
        tries     = combat['tries']
        attack    = combat_list(attackers, states)
        defend    = combat_list(defenders, states)
        af        = tries[0]['attackerStrength']
        df        = tries[0]['defenderStrength']
        aroll     = tries[0]['attackerRoll']
        droll     = tries[0]['defenderRoll']
        res       = tries[0]['result']
        ini       = ''
        if len(tries) > 1:
            aroll2  = tries[1]['attackerRoll']
            droll2  = tries[1]['defenderRoll']
            res2    = tries[1]['result']
            aroll   = shortstack(aroll,aroll2)
            droll   = shortstack(droll,droll2)
            res     = shortstack(res  ,res2  )
            inic    = 'initiative'+(' flipped' if init[0] == 'us' else '')
            ini     = shortstack(fr'\tikz[tab chit,scale=.7]{{\chit[{inic}]}}','')
            init[0] = 'us' if init[0] == 'cs' else 'cs'
            
        if no % 2 == 0:
            row_col(outp,no//2)
        e = r'\\' if no % 2 == 1 else '&'
        if ncombat == 1: e = r'\\'
        
        outp.write(fr'      {attack} & {fr} & {af} & {aroll} & {defend} & {to} & {df} & {droll} & {res} & {ini}{e}'+'\n')
    if ncombat != 1 and ncombat % 2 == 1:
        outp.write(r'    &&&&&&&&&\\'+'\n')
    outp.write(f'  \\end{{{env}}}}};\n')
    
# --------------------------------------------------------------------
def do_combats(outp,places,combats,turn,faction,eliminated,init):
    if not places: return 
    if not combats: return
    
    state(outp, places, turn, faction, 'After movement',init)
    for combat in combats:
        fr = combat.get('from',[])
        to = combat.get('to',[])
        
        if len(to) != 1:
            print(f'More then one target hex!: {to}')
            #continue
            
        for tt in to:
            to_coord = coord(tt)
            for ff in fr:
                from_coord = coord(ff)
                outp.write(fr'\draw[hex/attack]{from_coord}--{to_coord};'+'\n')

        fin  = combat.get('tries',[])[-1].get('result')
        outp.write(fr'\node[result] at {to_coord} {{{fin}}};')
        
        
    state_table(outp, places, turn, faction, 'After movement')
    # elim_table(outp, eliminated, turn, faction)
    combat_table(outp, combats, places, turn, faction,init)
    end_turn(outp)
    clearpage(outp)

# --------------------------------------------------------------------
def do_end(outp,combats,ends,turn,faction,eliminated,init):
    #if not combats: return 
    if not ends: return

    state(outp, ends, turn, faction, 'After combat',init)    
    state_table(outp, ends, turn, faction, 'After combat' )
    elim_table(outp, eliminated, turn, faction)
    end_turn(outp)

    clearpage(outp)
        
# --------------------------------------------------------------------
def do_faction(outp,phases,turn,faction,eliminated,init):
    places  = phases.get('moves',  None)
    combats = phases.get('combats',None)
    end     = phases.get('end',    None)
    # do_moves  (outp,
    #            places,
    #            turn,
    #            faction)
    facname = 'Confederate' if faction == 'cs' else 'Union'
    outp.write(fr'\pdfbookmark[1]{{{facname}}}{{{turn}@{facname}}}'+'\n')
    do_combats(outp,
               places,
               combats,
               turn,
               faction,
               eliminated,
               init)
    eliminated = phases.get('eliminated',None)
    do_end    (outp,
               combats,
               end,
               turn,
               faction,
               eliminated,
               init)
    
# ====================================================================
def test(outp,turn,faction='cs',phase='Test'):
    strat_turn(outp,turn,faction,phase)
    end_turn(outp)

# ====================================================================
def open_input(inp):
    from json import load

    # with open(filename,'r') as inp:
    return load(inp)

def check_steps(j):
    step = set()
    for turnNo, turn in j.items():
        for faction,phases in turn.items():
            moves =  phases['moves']
            for hex,units in moves.items():
                for unit,flip in units:
                    if not flip and unit in step:
                        print(f'{unit} changed stated to {flip} on turn {turnNo},{faction}')
                    elif flip:
                        step.add(unit)

# ====================================================================
def do_it(inname,outname,firstTurn=1,lastTurn=18):
    turns      = open_input(inname)
    outp       = open_out(outname)
    eliminated = None
    init       = ['cs']
    check_steps(turns)
    for turnNo, turn in turns.items():
        turnN = int(turnNo)
        print(turnN)
        outp.write(fr'\pdfbookmark[0]{{Turn {turnNo}}}{{{turnNo}}}'+'\n')
        if turnN < firstTurn:
            continue
        if turnN > lastTurn:
            break
        for faction,phases in turn.items():
            do_faction(outp,phases,turnN,faction,eliminated,init)
        
    close_out(outp)
    
# ====================================================================
if __name__ == '__main__':
    from argparse import ArgumentParser, FileType

    ap = ArgumentParser(description='Write LaTeX replay code')
    ap.add_argument('input',type=FileType('r'),nargs='?',
                    default='moves.json',help='Input file name')
    ap.add_argument('-o','--output',type=FileType('w'),nargs='?',
                    default='moves.tex',
                    help='output file name')
    ap.add_argument('-l','--last-turn',type=int,default=18,
                    help='Last turn to do')
    ap.add_argument('-f','--first-turn',type=int,default=1,
                    help='First turn to do')

    args = ap.parse_args()

    do_it(args.input,args.output,args.first_turn,args.last_turn)

#
# EOF
#

    
    
