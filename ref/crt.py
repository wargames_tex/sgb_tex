#!/usr/bin/env python

from numpy import arange, newaxis, where, logical_and, choose 


rolls = arange(-9,10)
diffs = arange(-10,11)
total = diffs[newaxis,:] + rolls[newaxis,:].T

index = \
    where(logical_and(total >= -5,
                      total <= -3),1,0) + \
    where(logical_and(total >= -2,
                      total <=  0),2,0) + \
    where(logical_and(total >=  1,
                      total <=  2),3,0) + \
   where(logical_and(total >=  3,
                       total <=  5),4,0) + \
   where(total >=  6,5,0)

choices = [['A2'],['A1'],['AR'],['DR'],['D1'],['D2']]
results = choose(index,choices)

#print(results.shape)

top   = '     '+f'{"Delta CF":>{(len(diffs)*4-1)//2+4}}'
left  = ' '*5 + 'Delta d10' + ' '*5
head  = '     '+' '.join([f'{d:3d}' for d in diffs])
table = '\n'.join([f'{l} {r:2d}  '+' '.join([f'{x:3s}' for x in row])
                   for r,row,l in zip(rolls,results,left)])

#print(top)
#print(head)
#print(table)


choices = [[r'\AT'],[r'\AO'],[r'\AR'],[r'\DR'],[r'\DO'],[r'\DT']]
results = choose(index,choices)

table = '\n  \\\\'.join([f'\n  {r"\defrow" if r%2==0 else r"\altrow"}'
                         +f'\n  \\textbf{{{r:d}}}\n  &'
                         +'\n  & '.join([f'{x}' for x in row])
                         for r,row in zip(rolls,results)])

with open('crt.tex','w') as crt:
    print(r'''
\documentclass{standalone}
\usepackage{colortbl}
\usepackage{multirow}
\usepackage{xcolor}
\usepackage{graphicx}
\colorlet{red15}{red!25!white}
\colorlet{red10}{red!15!white}
\colorlet{red5}{red!5!white}
\colorlet{green5}{green!5!white}
\colorlet{green10}{green!15!white}
\colorlet{green15}{green!25!white}
\colorlet{headcol}{gray!25!white}
\colorlet{altcol}{gray!5!white}
\newcommand\AT{\cellcolor{red15}A2}
\newcommand\AO{\cellcolor{red10}A1}
\newcommand\AR{\cellcolor{red5}AR}
\newcommand\DR{\cellcolor{green5}DR}
\newcommand\DO{\cellcolor{green10}D1}
\newcommand\DT{\cellcolor{green15}D2}
\newcommand\headrow{\rowcolor{headcol}}
\newcommand\defrow{\rowcolor{white}}
\newcommand\altrow{\rowcolor{altcol}}
\begin{document}
\begin{tabular}{|r|ccc|ccc|ccc|ccc|ccc|ccc|ccc|}
  \hline
  \headrow
  & \multicolumn{21}{c|}{$\mathbf{\Delta}$\textbf{CF}}\\[.5ex]
  \headrow
  \multirow{-2}*{\rotatebox{90}{$\mathbf{\Delta}$\textbf{d10}}}
    &'''+'\n  &'.join([fr'\textbf{{{d}}}' for d in diffs])
          +'\n'+'  \\\\[.5ex]\n'
          r'  \hline'+'\n'+table+'\n  \\\\\n'+
          r'''  \hline
\end{tabular}
\end{document}
''',file=crt)

