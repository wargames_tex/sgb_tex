# Gettysburg - Smithsonian edition

[[_TOC_]]

This is my remake of the wargame _Gettysburg - Smithsonian
edition_.  The original game was published by Avalon Hill Game Company
in 1988.  It was later republished as _Gettysburg - 125th anniversary
edition_ with no changes. 

The _Gettysburg - Smithsonian edition_ is a variant of the classic
AHGC
[Gettysburg](https://boardgamegeek.com/boardgame/3099/gettysburg) -
and [its monster successor](https://boardgamegeek.com/boardgame/1575/gettysburg),
geared toward new-comers to hex'n'counter board wargaming.  The game
was published, but has since gone out of print, as part of the
_American History_ series that AHGC developed together with the
Smithsonian Institute in United States of America.  Other games in the
series include

- [Battle of Bulge](https://gitlab.com/wargames_tex/sbotb_tex)
  simulating the infamous battle where the US commander stood his
  ground with the reply "Nuts" when encouraged to surrender to the
  Germans.
- [D-Day](https://gitlab.com/wargames_tex/sdday_tex)
  simulating Allied invasion of Western Europe in 1944.
- [Guadalcanal](https://boardgamegeek.com/boardgame/1375/guadalcanal)
  which was the start of the US pacific offensive.  This simulates
  mainly the naval part of the battle.
- [Midway](https://boardgamegeek.com/image/568159/midway) - the aerial
  battle of the islands in the middle of the Pacific Ocean 
- [Mustangs](https://boardgamegeek.com/image/6051696/mustangs) -
  dogfights during WWII. 
- [We the people](https://boardgamegeek.com/boardgame/620/we-people)
  Not really a hex'n'counter game, but a novel approach using cards
  (instead of dice) and interconnected spaces on a board.

See also Print'n'Play versions of the sister games [_D-Day -
Smithsonian Edition_](https://gitlab.com/wargames_tex/sdday_tex) and
[_Battle of the Bulge - Smithsonian
Edition_](https://gitlab.com/wargames_tex/sbotb_tex).

## Game mechanics 

|              |                    |
|--------------|--------------------|
| Period       | Gunpowder          |
| Level        | Operational        |
| Hex scale    | 640m (700 yards)   |
| Unit scale   | division (xx)      |
| Turn scale   | 2 hours            |
| Unit density | medium             |
| # of turns   | 24                 |
| Complexity   | 1 of 10            |
| Solitaire    | 8 of 10            |

Features:
- Campaign
- Scenarios
- Optional rules

## About this rework 

This rework is entirely new.  All text and graphics is new and nothing
is copied verbatim from the original materials. 

I have restructured and rewritten the rules somewhat (the text is all
new), as well as added illustrations of the rules. 


## The files 

The distribution consists of the following files 

- [GettysburgSmithsonianA4.pdf][] This is a single document that contains
  everything: The rules, charts, counters, and order of battle, and
  the board split into two parts.
  
  This document is meant to be printed on a duplex A4 printer.  The
  charts, OOB charts, counters, and board will be on separate sheets
  and can be separated from the rules.
  
- [GettysburgSmithsonianA4Booklet.pdf][] This document _only_ contains the
  rules.  It is meant to be printed on a duplex A4 printer with long
  edge binding.  This will produce 6 sheets which should be folded
  down the middle of the long edge and stabled to form an 24-page A5
  booklet of the rules.
  
- [GettysburgSmithsonianA3Booklet.pdf][] As above, but designed to be
  printed in an A3 duplex printer.  This will make a 6 sheet A4
  booklet. 
  
- [splitboardA3.pdf][] holds the entire board on a sheet of A3 paper.
  Print this and glue on to a sturdy piece of cardboard (1.5mm poster
  carton seems a good choice).
  
- [splitboardA4.pdf][] has the board split over two A4 sheets of
  paper.  Cut out and glue on to sturdy cardboard.  Note that one
  sheet should be cut at the crop marks and glued over the other
  sheet.  Take care to align the sheets.
  
- [materialsA4.pdf][] holds the charts, counters, and OOB.  It is
  meant to be printed on A4 paper.  Print and glue on to a relatively
  thick piece of cardboard (1.5mm or so) or the like.
  
  The counters are double-sided, which means one has to take care to
  align the sides.   My procedure is roughly as follows: 
  - I like to glue front of the counter tables on to a
	medium thick piece of cardboard (1.5mm poster carton) and let it
	dry between two heavy books. 
  - Then, once dried, I cut out the block using a sharp knife.
  - Then, I cut out the back of the counter table, and glue that on to
    the back of the cardboard with the front of the counters and let
    it dry between two heavy books.
  - Once dried I start to cut out the counters.  
    - I do that by cutting groves (i.e., not all the way through)
      along the vertical and horizontal lines.
	- Finally, I cut entirely through the cardboard along the longest
      direction, _except_ for a small bit in one end. 
    - Then, I can cut through in the other direction quite easily, and
      separate out the counters. 
  
If you only have access to US Letter (and possibly Tabloid) printer,
you can use the following files

| *A4 Series*                            | *Letter Series*                            |
| ---------------------------------------|--------------------------------------------|
| [GettysburgSmithsonianA4.pdf][]        | [GettysburgSmithsonianLetter.pdf][]  	  |
| [GettysburgSmithsonianA4Booklet.pdf][] | [GettysburgSmithsonianLetterBooklet.pdf][] |
| [GettysburgSmithsonianA3Booklet.pdf][] | [GettysburgSmithsonianTabloidBooklet.pdf][] |
| [materialsA4.pdf][]	                 | [materialsLetter.pdf][]	                  |
| [splitboardA4.pdf][]	                 | [splitboardLetter.pdf][]	                  |
| [splitboardA3.pdf][]	                 | [splitboardTabloid.pdf][]	              |

Download [artifacts.zip][] to get all files for both kinds of paper
formats. 

## VASSAL modules 

Also available is a [VASSAL](https://vassalengine.org) module

- [VMOD Scenario][GettysburgSmithsonian.vmod]

The module is generated from the same sources as the Print'n'Play
documents, and contains the rules as an embedded PDF.  The modules
features

- Battle markers 
- Automatic odds markers 
- Automatic battle resolution
- Optional rules selection
- Embedded rules 
- Moved and battle markers automatically cleared 
- Symbolic dice 
- Tutorial based on replay in [_The General_, **Vol.25**, #5, p6-14][]
- Contest 147 ([_The General_, **Vol 25**, #5, p16][]) and its
  solution ([_The General_, **Vol 25**, #6, p59][]) 

## Previews

![Board](.imgs/hexes.png)
![Charts](.imgs/tables.png)
![Counters](.imgs/chits.png)
![Front](.imgs/front.png)

![Union OOB Jul 1, 1863](.imgs/us-oob1.png)
![Union OOB Jul 2, 1863](.imgs/us-oob2.png)
![Union OOB Jul 3, 1863](.imgs/us-oob3.png)
![Union OOB organsiation](.imgs/us-org.png)

![Confederate OOB Jul 1, 1863](.imgs/cs-oob1.png)
![Confederate OOB Jul 2, 1863](.imgs/cs-oob2.png)
![Confederate OOB Jul 3, 1863](.imgs/cs-oob3.png)
![Confederate organisation](.imgs/cs-org.png)

![Setup 2nd of July](.imgs/setup-july2.png)
![Setup 3rd of July](.imgs/setup-july3.png)

![VASSAL module](.imgs/vassal.png)

![Components](.imgs/photo_components.png)
![Turn 3](.imgs/photo_turn03.jpg)
![Turn 7](.imgs/photo_turn07.jpg)
![Turn 11](.imgs/photo_turn11.jpg)
![End of first day - a bad day for the confederacy](.imgs/photo_badday.png)

(Note, some of the photos are slightly out of date)


## Articles 

* Martin,R.A., Cluck,B.A., and Taylor,S.C., Martin, B., ``Gettysburg
  '88 (Series Replay)'', [_The General_, **Vol.25**, #5, p6-14][]
* Boeche,T., ``They Led at Gettysburg - The Confederate and Union
  Generals'', [_The General_, **Vol.25**, #5, p15-16][]
* ``Contest 147", [_The General_, **Vol 25**, #5, p16][]
* Blumberg,A., ``Horse Soldiers - Cavalry Operations during the
  Gettysburg Campaign'', [_The General_, **Vol.25**, #5, p17-19][]
* Marian,J. \& Brown,J., ``Intermediate GETTYSBURG '88'', [_The
  General_, **Vol.25**, #5, p19][]
* Moore,O., ``Let us cross over the river - A Flight of Fancy for
  Wargamers'', [_The General_, **Vol.25**, #5, p20-24][]
* ``The Question Box'', [_The General_, **Vol.25**, #5, p58][]
* ``Solution to contest 147'', [_The General_, **Vol 25**, #6, p59][]
* Mones,S., ``Three days in Pennsylvania - Optional rules for
  Gettysburg (125th Anniversary Edition)'', [_The General_,
  **Vol.29**, #4, p35-38][]
*  Avaloncon GM, ``Rules Clarifications'', 
  [_The Boardgamer_, **Vol 1**, #4, p22-23][], 1996
* Freeman, R., ``Union opening strategy'', 
  [_The Boardgamer_, **Vol 2**, #1, p27-28][], 1997
* Thomson, B., [_Insight into Gettysburg '88 - Tournament play for the
novice_][], Unpublished

Also 

<iframe 
  width="560" 
  height="315" 
  src="https://www.youtube.com/embed/dSgrvOBwpJ4?si=hglal2mt3ZoJLiSs" 
  title="YouTube video player" 
  frameborder="0" 
  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
  referrerpolicy="strict-origin-when-cross-origin"
  allowfullscreen>
</iframe>

- ["Gettysburg '88 (Series Replay)" with this P'n'P][] video
- [YouTube introduction][] by [Legendary Tactics][]
- ["Gettysburg '88 (Series Replay)" with original board][] by [Legendary Tactics][]
- [Errata][] to original _Gettysburg '88_ (incorporated in _Gettysburg '91_)
- [FAQ errata][] to original (from ``The Question Box'', [_The General_, **Vol.25**, #5, p58]) 
- [Map clarifications][] (_not_ official)
- [Terrain clarifications][] (_not_ official)

## Implementation 

The whole package is implemented in LaTeX using my package
[_wargame_](https://gitlab.com/wargames_tex/wargame_tex).  This
package, combined with the power of LaTeX, produces high-quality
documents, with vector graphics to ensure that everything scales.

[artifacts.zip]: https://gitlab.com/wargames_tex/sgb_tex/-/jobs/artifacts/master/download?job=dist

[GettysburgSmithsonianA4.pdf]: https://gitlab.com/wargames_tex/sgb_tex/-/jobs/artifacts/master/file/GettysburgSmithsonian-A4-master/GettysburgSmithsonianA4.pdf?job=dist
[GettysburgSmithsonianA4Booklet.pdf]: https://gitlab.com/wargames_tex/sgb_tex/-/jobs/artifacts/master/file/GettysburgSmithsonian-A4-master/GettysburgSmithsonianA4Booklet.pdf?job=dist
[GettysburgSmithsonianA3Booklet.pdf]: https://gitlab.com/wargames_tex/sgb_tex/-/jobs/artifacts/master/file/GettysburgSmithsonian-A4-master/GettysburgSmithsonianA3Booklet.pdf?job=dist
[materialsA4.pdf]: https://gitlab.com/wargames_tex/sgb_tex/-/jobs/artifacts/master/file/GettysburgSmithsonian-A4-master/materialsA4.pdf?job=dist
[splitboardA3.pdf]: https://gitlab.com/wargames_tex/sgb_tex/-/jobs/artifacts/master/file/GettysburgSmithsonian-A4-master/splitboardA3.pdf?job=dist
[splitboardA4.pdf]: https://gitlab.com/wargames_tex/sgb_tex/-/jobs/artifacts/master/file/GettysburgSmithsonian-A4-master/splitboardA4.pdf?job=dist
[GettysburgSmithsonian.vmod]: https://gitlab.com/wargames_tex/sgb_tex/-/jobs/artifacts/master/file/GettysburgSmithsonian-A4-master/GettysburgSmithsonian.vmod?job=dist


[GettysburgSmithsonianLetter.pdf]: https://gitlab.com/wargames_tex/sgb_tex/-/jobs/artifacts/master/file/GettysburgSmithsonian-Letter-master/GettysburgSmithsonianLetter.pdf?job=dist
[GettysburgSmithsonianLetterBooklet.pdf]: https://gitlab.com/wargames_tex/sgb_tex/-/jobs/artifacts/master/file/GettysburgSmithsonian-Letter-master/GettysburgSmithsonianLetterBooklet.pdf?job=dist
[GettysburgSmithsonianTabloidBooklet.pdf]: https://gitlab.com/wargames_tex/sgb_tex/-/jobs/artifacts/master/file/GettysburgSmithsonian-Letter-master/GettysburgSmithsonianTabloidBooklet.pdf?job=dist
[materialsLetter.pdf]: https://gitlab.com/wargames_tex/sgb_tex/-/jobs/artifacts/master/file/GettysburgSmithsonian-Letter-master/materialsLetter.pdf?job=dist
[splitboardTabloid.pdf]: https://gitlab.com/wargames_tex/sgb_tex/-/jobs/artifacts/master/file/GettysburgSmithsonian-Letter-master/splitboardTabloid.pdf?job=dist
[splitboardLetter.pdf]: https://gitlab.com/wargames_tex/sgb_tex/-/jobs/artifacts/master/file/GettysburgSmithsonian-Letter-master/splitboardLetter.pdf?job=dist

[_The General_, **Vol.25**, #5, p6-14]: https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2025%20No%205.pdf#page=6
[_The General_, **Vol.25**, #5, p15-16]: https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2025%20No%205.pdf#page=15
[_The General_, **Vol 25**, #5, p16]: https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2025%20No%205.pdf#page=16
[_The General_, **Vol.25**, #5, p17-19]: https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2025%20No%205.pdf#page=17
[_The General_, **Vol.25**, #5, p19]: https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2025%20No%205.pdf#page=19
[_The General_, **Vol.25**, #5, p20-24]: https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2025%20No%205.pdf#page=20
[_The General_, **Vol.25**, #5, p58]: https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2025%20No%205.pdf#page=58
[_The General_, **Vol 25**, #6, p59]: https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2025%20No%206.pdf#page=59
[_The General_, **Vol.29**, #4, p35-38]: https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2029%20No%204.pdf#page=35
[_The Boardgamer_, **Vol 2**, #1, p27-28]: https://www.scribd.com/document/383621273/Boardgamer-v2n1
[_The Boardgamer_, **Vol 1**, #4, p22-23]: https://www.scribd.com/document/383621224/Boardgamer-v1n4
[_Insight into Gettysburg '88 - Tournament play for the
novice_]: http://wargameacademy.org/ARTICLES/G88-Novice-Article-090506.pdf

[Errata]: https://www.grognard.com/wargamerfiles/getty88.gif
[FAQ errata]: https://www.grognard.com/errata/getty88.txt
[Map clarifications]: https://gbg88game.com/clarifications/clarifications.html
[Terrain clarifications]: https://gbg88game.com/terrainclarifications/images/GBG%20Terrain%20Clarifications.pdf
[YouTube introduction]: https://youtu.be/FWSKvnowI9E
["Gettysburg '88 (Series Replay)" with original board]: https://youtu.be/0bCBZh3qYns
["Gettysburg '88 (Series Replay)" with this P'n'P]: https://youtu.be/dSgrvOBwpJ4
[Legendary Tactics]: https://www.youtube.com/@LegendaryTactics
