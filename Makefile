#
#
#
NAME		:= GettysburgSmithsonian
VERSION		:= 1.0-3
VMOD_VERSION	:= 1.3.0
VMOD_TITLE	:= Gettysburg - Smithsonian

LATEX		:= lualatex
CONVERT		:= convert
CONVERT_FLAGS	:= -trim 

DOCFILES	:= $(NAME).tex			\
		   front.tex			\
		   preface.tex			\
		   rules.tex			\
		   materials.tex		
DATAFILES	:= hexes.tex			\
		   oob.tex			\
		   chits.tex			\
		   tables.tex			\
		   org.tex			\
		   probabilities.tex		\
		   wargame.speckle.tex

OTHER		:= 
STYFILES	:= sgb.sty			\
		   commonwg.sty
BOARD		:= hexes.pdf
BOARDS		:= splitboardA4.pdf
SIGNATURE	:= 20
PAPER		:= --a4paper
TARGETS		:= $(NAME).pdf 			\
		   $(NAME)Booklet.pdf 		\
		   materials.pdf		\
		   splitboardA3.pdf		\
		   hexes.pdf

TARGETSA4	:= $(NAME)A4.pdf 		\
		   $(NAME)A4Booklet.pdf 	\
		   $(NAME)A3Booklet.pdf 	\
		   materialsA4.pdf		\
		   hexes.pdf			\
		   splitboardA3.pdf		\
		   splitboardA4.pdf			

TARGETSLETTER	:= $(NAME)Letter.pdf 		\
		   $(NAME)LetterBooklet.pdf 	\
		   $(NAME)TabloidBooklet.pdf 	\
		   materialsLetter.pdf		\
		   splitboardTabloid.pdf	\
		   splitboardLetter.pdf		
SUBMAKE_FLAGS	:= --no-print-directory

ifdef VERBOSE	
MP4_FLAGS	:= -v
endif

include Variables.mk
include Patterns.mk

all:	$(TARGETS)
a4:	$(TARGETSA4) vmod
letter:	$(TARGETSLETTER)
vmod:	$(NAME).vmod
mine:	a4 cover.pdf box.pdf org.pdf

.imgs/chits.png:chitsA4.png
	@echo "CONVERT		$@"
	$(MUTE)$(CONVERT) $< $(CONVERT_FLAGS) $@

.imgs/hexes.png:hexes.png
	@echo "CONVERT		$@"
	$(MUTE)$(CONVERT) $< $(CONVERT_FLAGS) $@

.imgs/tables.png:tables.png
	@echo "CONVERT		$@"
	$(MUTE)$(CONVERT) $< $(CONVERT_FLAGS) $@

.imgs/front.png:front.png
	@echo "CONVERT		$@"
	$(MUTE)$(CONVERT) $< $(CONVERT_FLAGS) $@

.imgs/logo.png:logo.png
	@echo "MV		$@"
	$(MUTE)mv $< $@

.imgs/us-oob1.png:oobA4.pdf
	@echo "CAIRO		$<"
	$(MUTE)$(PDFTOCAIRO) $(CAIROFLAGS) -png $<
	$(MUTE)$(CONVERT) oobA4-01.png $(CONVERT_FLAGS) .imgs/us-oob1.png
	$(MUTE)$(CONVERT) oobA4-03.png $(CONVERT_FLAGS) .imgs/us-oob2.png
	$(MUTE)$(CONVERT) oobA4-05.png $(CONVERT_FLAGS) .imgs/us-oob3.png
	$(MUTE)$(CONVERT) oobA4-07.png $(CONVERT_FLAGS) .imgs/cs-oob1.png
	$(MUTE)$(CONVERT) oobA4-09.png $(CONVERT_FLAGS) .imgs/cs-oob2.png
	$(MUTE)$(CONVERT) oobA4-11.png $(CONVERT_FLAGS) .imgs/cs-oob3.png
	$(MUTE)rm -f oobA4*.png

.imgs/us-org.png:org.pdf
	@echo "CAIRO		$<"
	$(MUTE)$(PDFTOCAIRO) $(CAIROFLAGS) -png $<
	$(MUTE)$(CONVERT)  org-1.png $(CONVERT_FLAGS) .imgs/us-org.png
	$(MUTE)$(CONVERT)  org-2.png $(CONVERT_FLAGS) .imgs/cs-org.png
	$(MUTE)rm -f org*.png

.imgs/setup-july2.png:setupA3.pdf
	@echo "CAIRO		$<"
	$(MUTE)$(PDFTOCAIRO) $(CAIROFLAGS) -png $<
	$(MUTE)$(CONVERT) setupA3-1.png $(CONVERT_FLAGS) .imgs/setup-july2.png
	$(MUTE)$(CONVERT) setupA3-2.png $(CONVERT_FLAGS) .imgs/setup-july3.png
	$(MUTE)rm -f setupA3*.png

.imgs/setup-july3.png:.imgs/setup-july2.png
	if test -f $< ; then rm $< ; $(MAKE) $< ; fi 

update-images:	.imgs/chits.png 	\
		.imgs/hexes.png 	\
		.imgs/us-oob1.png 	\
		.imgs/tables.png	\
		.imgs/front.png		\
		.imgs/logo.png		\
		.imgs/us-org.png	\
		.imgs/setup-july2.png	\
		.imgs/setup-july3.png	

include Rules.mk

clean::
	$(MUTE)rm -f chapters.tex tmp_*

realclean::
	$(MUTE) rm -f moves.tex *.mp4 *.webp *.webm splitboard*.*

# These generate images that are common for all formats 
hexes.pdf:		hexes.tex	wargame.speckle.pdf $(STYFILES)
export.pdf:		export.tex	$(DATAFILES)	$(STYFILES) \
			hexes.pdf	probabilities.pdf
wargame.speckle.pdf:	wargame.speckle.tex

$(NAME).pdf:		$(NAME).aux
$(NAME).aux:		$(DOCFILES) 	$(STYFILES)	$(BOARDS)
materials.pdf:		materials.tex	$(DATAFILES)	$(BOARDS)	\
			probabilities.pdf

$(NAME)A4.pdf:		$(NAME)A4.aux
$(NAME)A4.aux:		$(DOCFILES) 	$(STYFILES) 	$(BOARDS)	\
			probabilities.pdf
materialsA4.pdf:	materials.tex 	$(DATAFILES) 	$(BOARDS)	\
			probabilities.pdf
oobA4.pdf:		oob.tex				$(STYFILES)

$(NAME)Letter.pdf:	$(NAME)Letter.aux
$(NAME)Letter.aux:	$(DOCFILES) 	$(STYFILES) 	splitboardLetter.pdf \
			probabilities.pdf
materialsLetter.pdf:	materials.tex 	$(DATAFILES) 	splitboardLetter.pdf \
			probabilities.pdf
oobLetter.pdf:		oob.tex				$(STYFILES)

$(NAME)Rules.pdf:	$(NAME)Rules.aux
$(NAME)Rules.aux:	$(DOCFILES) 	$(STYFILES) 	$(BOARDS)

# Generic dependency

frontA4.pdf:		front.tex       $(STYFILES)
chitsA4.pdf:		chits.tex       $(STYFILES)
$(NAME)LetterBooklet.pdf:PAPER=--letterpaper
$(NAME)LetterBooklet.pdf:SIGNATURE=20

probtable.tex:		ref/probabilities.py
	python $< > $@

$(NAME).vmod:		TUTORIAL:=ReplayTGvol25no5p6.vlog

moves.json:
	@echo "PYTHON		$@"
	$(MUTE)./ref/read_moves.py $(REDIR)

moves.tex:	moves.json
	@echo "PYTHON		$@"
	$(MUTE)./ref/write_moves.py $(REDIR)

movesA4.pdf:movesReorg.tex movesA3.pdf tables.tex
	@echo "LATEX   	movesA4"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $(basename $@) $< $(REDIR)

movesLetter.pdf:movesReorg.tex movesTabloid.pdf tables.tex
	@echo "LATEX   	movesLetter"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $(basename $@) $< $(REDIR)

%.mp4:	%A3.pdf
	@echo "PYTHON		$@"
	$(MUTE)./ref/make_movie.py $< $@ $(MP4_FLAGS)

%.webp:	%A3.pdf
	@echo "PYTHON		$@"
	$(MUTE)./ref/make_movie.py $< $@ $(MP4_FLAGS)

movesA3.pdf:		moves.tex
movesTabloid.pdf:	moves.tex
movesA4Booklet.pdf:	SIGNATURE=128
movesTabloidBooklet.pdf:SIGNATURE=128
setupA3.pdf:		setup.tex hexes.pdf
setupTabloid.pdf:	setup.tex hexes.pdf
openingA3.pdf:		opening.tex hexes.pdf
openingTabloid.pdf:	opening.tex hexes.pdf
opening.mp4:		MP4_FLAGS+=-F .25
opening.webp:		MP4_FLAGS+=-F .25

include Docker.mk

docker-artifacts::
	cp $(NAME).vmod $(NAME)-A4-$(VERSION)/

.PRECIOUS:	export.pdf $(NAME)Rules.pdf $(NAME).vtmp

#
# EOF
#

